import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import * as moment from 'moment';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class Servicios{
  Cargando: boolean = false;
  UsuMat: any = {};

  App: any = {
    Nombre : "TravElct"
  }

  Pais: any = {
    "Nombre":"Mexico",
    "Codigo":"+52",
    "Bandera":"banmex.png",
    "TerCon":"tercon.pdf",
    "SimMon":"$",
    "Lat":"19.433021004809355",
    "Lon":"-99.13516746685117"
  }

  //ApiUrl: string = "http://35.238.26.205/travelct/";
  ApiUrl: string = "https://travelct.mx/travelct/";

  DGF: any = {};

  DatNot: any = {
    "Total":[],
    "Soporte":[],
    "Conductores":[]
  }

  constructor(
    public http: HttpClient,
    public snackBar: MatSnackBar
  ){
  }

  async AccSobBDAA(Acc,Res,Cam,Val,CyV,Tab,Don,Ord,Sen,MosLoa){
    if (MosLoa){this.Cargando = true;}
    let JSONSend:any = {"acc":Acc,"res":Res,"cam":Cam,"val":Val,"cyv":CyV,"tab":Tab,"don":Don,"ord":Ord,"sen":Sen,"tok":window.localStorage.getItem("Token")};
    JSONSend = JSON.stringify(JSONSend);
    //console.log(JSONSend);

    let body:string = "jsonsend="+JSONSend;
    let headers:any = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"});
    let options:any = ({headers:headers});
    let url:any = this.ApiUrl + "api/admbd.php";

    let dataRes = await this.http.post(url,body,options).toPromise();
    this.Cargando = false;
    //console.log(dataRes);
    return dataRes;
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
     const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
     FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  ColEstVia(Estatus){
    switch(Estatus){
      case "Pendiente":
          return "#f25213";
      case "Nueva":
        return "#60d49b";
      case "Pagada":
        return "#29a5dc";
      case "Aceptada":
        return "#29a5dc";
      case "Asignada":
        return "#29a5dc";
      case "Cerca":
        return "#32cf32";
      case "Iniciada":
        return "#f29b13";
      case "Finalizada":
        return "#8832cf";
      case "Calificado Conductor":
        return "#8832cf";
      case "Cancelada Conductor":
        return "#cf5132";
      case "Calificado Cliente":
        return "#8832cf";
      case "Cancelado Cliente":
        return "#cf5132";
      case "Cancelado Conductor":
          return "#cf5132";
      case "Terminado":
        return "#32ad32";
      case "En Devolucion":
        return "#f29b13";
      case "En Centro":
        return "#8832cf";
      case "Devuelto":
        return "#e99047";
    }
  }

  ColMon(Monto){
    if (Monto < 1){
      return "#f29b13";
    }else{
      return "#32cf32";
    }
  }

  ColSal(Saldo){
    if (Saldo < 0){
      return "#cf5132";
    }else{
      return "#32cf32";
    }
  }

  EnvMsgSim(Men,Color){
    //alert(Men);
    var CSB
    switch (Color) {
      case "Normal":
        CSB = 'snackbar-normal'
      break;

      case "Informacion":
        CSB = 'snackbar-importante'
      break;

      case "Hecho":
        CSB = 'snackbar-funciono'
      break;

      case "Advertencia":
        CSB = 'snackbar-advertencia'
      break;

      case "Peligro":
        CSB = 'snackbar-error'
      break;

      default:
        CSB = 'snackbar-normal'
      break;
    }
    this.snackBar.open(Men,"",{duration: 3000,panelClass: [CSB]});
  }

  AliCamMod(Tipo){
    switch(Tipo){
      case "Texto":
        return "left";
      case "Entero":
        return "right";
      case "Numero":
        return "right";
      case "Porcentaje":
        return "right";
      case "Select":
        return "center";
      case "SelectQry":
        return "left";
      case "Fecha":
        return "center";
      case "FechaHora":
        return "center";
      case "Imagen":
        return "center";
      case "Documento":
        return "center";
      case "Wallet":
        return "right";
      case "Geocerca":
        return "center";
      case "NumTarCre":
        return "center";
      case "VenTarCre":
        return "center";
      case "CodTarCre":
        return "center";
    }
  }

  ForCamMod(Valor,Cmp){
    switch(Cmp.Tip){
      case "Texto":
        return Valor;
      case "Entero":
        return Valor;
      case "Numero":
        return this.NumFor(Valor,2);
      case "Porcentaje":
        return this.NumFor(Valor,2);
      case "Select":
        return Valor;
      case "SelectQry":
        var MCMP = Cmp.Qry.Cmps.split(",");
        for (var n = 0; n < Cmp.Qry.Res.length; n++){
          if (Cmp.Qry.Res[n][MCMP[0]] == Valor){
            if (Cmp.Qry.Res[n][MCMP[2]]){
              return Cmp.Qry.Res[n][MCMP[1]]+" "+Cmp.Qry.Res[n][MCMP[2]];
            }else{
              return Cmp.Qry.Res[n][MCMP[1]];
            }
          }
        }
        return "???";

      case "Fecha":
        return this.ForFec(Valor);
      case "FechaHora":
        return this.ForFecHorPeq(Valor);
        //return this.ForFecHor(Valor);
      case "Imagen":
        return Valor;
      case "Documento":
        return Valor;
      case "Wallet":
        let ColWal = "";
        if (Valor == 0){
          ColWal = "#000";
        }else if (Valor < 0){
          ColWal = "#900";
        }else if (Valor > 0){
          ColWal = "#090";
        }
        return this.NumFor(Valor,2);

      case "NumTarCre":
        return "****-****-****-"+Valor.substr(-4);
      case "VenTarCre":
        return Valor;
      case "CodTarCre":
        return "**"+Valor.substr(-1);

      case "Geocerca":
        let re1 = /\)\!\(/gi;
        let re2 = /\(/gi;
        let re3 = /\)/gi;

        let Puntos = Valor.replace(" ","");
        Puntos = Puntos.replace(re1,"|");
        Puntos = Puntos.replace(re2,"");
        Puntos = Puntos.replace(re3,"");
    }
  }

  NumFor(amount, decimals){
    let monori = amount;
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0){
      //return parseFloat('0').toFixed(decimals);
      if (decimals == 0){
        return '0';
      }else{
        return '0,'+'0'.repeat(decimals);
      }
    }
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');
    
    
    if (monori > 0){
      return amount_parts.join(',');
    }else{
      return "-"+amount_parts.join(',');
    }
  }

  ForFec(Fec){
    var MF = Fec.split("-");
    return MF[2]+"-"+MF[1]+"-"+MF[0];
  }

  ForFecHor(FecHor){
    var MFH = FecHor.split(" ");
    var MF = MFH[0].split("-");
    return MF[2]+"-"+MF[1]+"-"+MF[0]+" "+MFH[1];
  }

  ForFecHorPeq(FecHor){
    var MFH = FecHor.split(" ");
    var MF = MFH[0].split("-");
    var MH = MFH[1].split(":");
    return MF[2]+"-"+MF[1]+"-"+MF[0].substr(-2)+" "+MH[0]+":"+MH[1];
  }

  ForFecHorFer(Fec){
    var MF = Fec.split("-");
    return MF[2]+"-"+MF[1];
  }

  ValCam(objX,nombre,tipo,valor,requerido){
    var obj = document.getElementById(objX);
    if (requerido == "Si" && (valor === "" || valor === null)){
      this.EnvMsgSim("El campo "+nombre+" es requerido","Advertencia");

      obj.style.backgroundColor = "#fff3cd";
      return false;
    }else{
      obj.style.backgroundColor = "#fff";
    }
    
    var filter=/^[A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
    var filter2=/^[A-Za-z]*[0-9]/;
    var filter3=/^[A-Za-z]/;
    //var filter4=/^[0-9]$/;
    //var filter5=/^([0-2]\d):([0-5]\d):([0-5]\d)$/;
    //var filter6=/\d{3}(.\d{2})(.\d{2})(.\d{2})/;
    //var filter7=/^([0-9]){11}$/;
    var filter8=/^([0-9]){7,14}$/;
    var filter9=/^([0-9]){16}$/;
    var filter10=/^([0-9]{2})\/([0-9]{2})$/;
    var filter11=/^([0-9]){3}$/;
        
    switch (tipo){
      case "Texto":
        obj.style.backgroundColor = "#fff";
        return true;

      case "Cedula":
        if (filter2.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser una cédula válida","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "Telefono":
        if (filter8.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser un teléfono válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }
      
      case "Email":
        if (filter.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser un email válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "SolLet":
        if (filter3.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser solo letras","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "SolNum":
        if (!isNaN(valor)){
            obj.style.backgroundColor = "#fff";
            return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser solo números","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "Entero":
        if (isNaN(parseInt(valor))){
          this.EnvMsgSim("El campo "+nombre+" debe ser un número entero válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }else{
          obj.style.backgroundColor = "#fff";
          return true;
        }

      case "Numero":
        if (isNaN(parseFloat(valor))){
          this.EnvMsgSim("El campo "+nombre+" debe ser un número decimal válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }else{
          obj.style.backgroundColor = "#fff";
          return true;
        }

      case "Decimal":
        if (isNaN(parseFloat(valor))){
          this.EnvMsgSim("El campo "+nombre+" debe ser un número decimal válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }else{
          obj.style.backgroundColor = "#fff";
          return true;
        }

      case "Porcentaje":
        if (isNaN(parseFloat(valor))){
          this.EnvMsgSim("El campo "+nombre+" debe ser un Porcentaje válido","Advertencia");
          return false;
        }else{
          if (parseFloat(valor) > -1 && parseFloat(valor) < 101){
            return true;
          }else{
            this.EnvMsgSim("El campo "+nombre+" debe ser un Porcentaje válido","Advertencia");
            return false;
          }
        }

      case "NumTarCre":
        if (filter9.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser un numero de tarjeta válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "VenTarCre":
        if (filter10.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser una fecha de vencimiento válida MM/AA","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      case "CodTarCre":
        if (filter11.test(valor)){
          obj.style.backgroundColor = "#fff";
          return true;
        }else{
          this.EnvMsgSim("El campo "+nombre+" debe ser un código de seguridad válido","Advertencia");
          obj.style.backgroundColor = "#fff3cd";
          return false;
        }

      default:
        obj.style.backgroundColor = "#fff";
        return true;
    }
  }
}