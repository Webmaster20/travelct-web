import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from '../../servicios/servicios';

@Component({
  selector: 'app-mansermen',
  templateUrl: './mansermen.component.html',
  styleUrls: ['./mansermen.component.scss']
})
export class MansermenComponent implements OnInit {

  MemPar: string = "";
  Titulo: string = "";
  Mensaje: string = "";

  constructor(
    public router: Router,
    public servicios: Servicios
  ){
  }

  ngOnInit() {
  }

  Enviar(){
    if (!this.servicios.ValCam("MemPar","Mensaje para","Texto",this.MemPar,"Si")){return;}
    if (!this.servicios.ValCam("Titulo","Titulo","Texto",this.Titulo,"Si")){return;}
    if (!this.servicios.ValCam("Mensaje","Mensaje","Texto",this.Mensaje,"Si")){return;}

    var FilUsu: string = "";
    switch(this.MemPar){
      case "Clientes":
        FilUsu = "WHERE Tipo = |Cliente|";
      break;
      case "Conductores":
        FilUsu = "WHERE Tipo = |Conductor|";
      break;
      case "Todos":
        FilUsu = "WHERE Tipo IN (|Cliente|,|Conductor|)";
      break;
    }

    this.servicios.AccSobBDAA("EJECUTA","No","","","","","","","INSERT INTO meneme (NRegUsu,Titulo,Mensaje,FecHor) SELECT NRegistro,|"+this.Titulo+"|,|"+this.Mensaje+"|,NOW() FROM usuarios "+FilUsu,true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);

      this.MemPar = "";
      this.Titulo = "";
      this.Mensaje = "";

      this.servicios.EnvMsgSim("El Mensaje Emergente fue enviado a los usuarios","Informacion");

    }).catch((err)=>{console.log(err)});
    /*

    this.servicios.AccSobBD("EJECUTA","No","*","","","","","","INSERT INTO meneme (NRegUsu,Titulo,Mensaje,FecHor) SELECT NRegistro,|"+this.Titulo+"|,|"+this.Mensaje+"|,NOW() FROM usuarios "+FilUsu,"Sen",this,
      function(dataRes,thiss){
        //console.log(dataRes);

        thiss.MemPar = "";
        thiss.Titulo = "";
        thiss.Mensaje = "";

        thiss.servicios.EnvMsgSim("El Mensaje Emergente fue enviado a los usuarios","Informacion");
      }
    );
    */
  }
}