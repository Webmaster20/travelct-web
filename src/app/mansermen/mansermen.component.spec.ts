import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MansermenComponent } from './mansermen.component';

describe('MansermenComponent', () => {
  let component: MansermenComponent;
  let fixture: ComponentFixture<MansermenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MansermenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MansermenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
