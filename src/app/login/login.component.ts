import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email: string = "";
  passw: string = "";
  token: string = "";

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit(): void {
    //window.localStorage.setItem("token","");
    this.token = window.localStorage.getItem("token");
    //console.log("***"+this.token+"***");
    if (this.token != "" && this.token != null && this.token != undefined){this.Login();}
  }



  ngAfterViewInit(){
    this.servicios.Cargando = false;
  }

  Login(){
    if(this.email != "" && this.passw != "" || this.token != ""){
      this.servicios.AccSobBDAA("LOGINADM","Si","",this.email+","+this.passw+","+this.token,"","","","","",true).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
  
        this.email = "";
        this.passw = "";
  
        if (Res.data.length > 0){
          window.localStorage.setItem("Token",Res.data[0].Token);
          switch(Res.data[0].Estatus){
            case "Activo":
              window.localStorage.setItem("token",Res.data[0].Token);
              this.servicios.UsuMat = Res.data[0];
              this.router.navigate(["home"]);
            break;
  
            case "Nuevo":
              this.servicios.EnvMsgSim("Usuario Nuevo","Advertencia");
            break;
            
            case "Bloqueado":
              this.servicios.EnvMsgSim("Usuario Bloqueado","Peligro");
            break;
          }
        }else{
          this.servicios.EnvMsgSim("Usuario y/o contraseña invalida","Advertencia");
        }
      }).catch((err)=>{console.log(err)});
    }else{
      this.servicios.EnvMsgSim("Por favor complete los campos","Advertencia");
    }
  }

  RecCon(){

  }
}
