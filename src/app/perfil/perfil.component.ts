import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';

import { FileUploader } from 'ng2-file-upload';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  @ViewChild('InpFot') InpFot: ElementRef;

  Cargando: string = "";
  uploader: FileUploader = new FileUploader({url:this.servicios.ApiUrl+"img/usu/upload.php", removeAfterUpload:false, autoUpload:true});

  constructor(public router: Router,public routeAct: ActivatedRoute, public servicios: Servicios) {
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      var responsePath = JSON.parse(response);
      console.log(responsePath)
      this.servicios.UsuMat.FotPer = responsePath.name;
      this.Cargando = "";
    };
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
    });
  }

  CarFot(){
    this.InpFot.nativeElement.click();
  }
  ActVal(){
    this.Cargando = "Si";
  }

  ActPer(){
    if (!this.servicios.ValCam("Nombre","Nombre","Texto",this.servicios.UsuMat.Nombre,"Si")){return;}
    if (!this.servicios.ValCam("Apellido","Apellido","Texto",this.servicios.UsuMat.Apellido,"Si")){return;}
    if (!this.servicios.ValCam("Email","Email","Email",this.servicios.UsuMat.Email,"Si")){return;}
    if (!this.servicios.ValCam("Telefono","Teléfono","Telefono",this.servicios.UsuMat.Telefono,"Si")){return;}

    this.servicios.AccSobBDAA("UPDATE","No","","","Nombre=|"+this.servicios.UsuMat.Nombre+"|,Apellido=|"+this.servicios.UsuMat.Apellido+"|,Email=|"+this.servicios.UsuMat.Email+"|,Telefono=|"+this.servicios.UsuMat.Telefono+"|,FotPer=|"+this.servicios.UsuMat.FotPer+"|","usuarios","WHERE NRegistro="+this.servicios.UsuMat.NRegistro,"","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("Los datos fueron actualizados","Hecho");
    }).catch((err)=>{console.log(err)});
  }
}
