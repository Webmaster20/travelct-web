import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManserdocComponent } from './manserdoc.component';

describe('ManserdocComponent', () => {
  let component: ManserdocComponent;
  let fixture: ComponentFixture<ManserdocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManserdocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManserdocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
