import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-manserdoc',
  templateUrl: './manserdoc.component.html',
  styleUrls: ['./manserdoc.component.scss']
})
export class ManserdocComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  DetDat: any;

  DGM: any = {
    "Nombre":"Nuevos / Documentos Recibidos",
    "CamMod":[
      {"Tit":"Conductor","Cmp":"NRegCon","Tip":"SelectQry","Req":"Si","VD":"","Val":"","Blo":"No","Qry":{"Cmps":"NRegistro,Nombre,Apellido","Tab":"usuarios","Don":"WHERE Tipo = |Conductor|","Ord":"ORDER BY Nombre,Apellido","Res":[]}},
      {"Tit":"Documento","Cmp":"NRegDoc","Tip":"SelectQry","Req":"Si","VD":"","Val":"","Blo":"No","Qry":{"Cmps":"NRegistro,Descripcion","Tab":"docreq","Don":"WHERE Estatus = |Activo|","Ord":"ORDER BY NRegistro","Res":[]}},
      {"Tit":"Documento","Cmp":"Documento","Tip":"Documento","Req":"Si","VD":"fpdc.png","Val":"","Est":"","Car":"usu"},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activa","Tex":"Activa"},{"Val":"Inactiva","Tex":"Inactiva"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"docreqcon",
    "Donde":"WHERE Estatus=|Nuevo|",
    "Ordena":"ORDER BY NRegCon,NRegDoc",
    "TabFor":{"Fil":[1,2],"Col":[1,2]},
    "Carpeta":"usu"
  };

  constructor(
    public router: Router,
    public servicios: Servicios,
    public dialog: MatDialog
  ) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarCmpQry();
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  AprDoc(Dat){
    this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Aprobado|","docreqcon","WHERE NRegistro="+Dat.NRegistro,"","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("El Documento fue Aprobado","Hecho");
      this.CarLis();
      this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) TotDocReq","","","docreq","WHERE Estatus=|Activo|","","",true).then((dataRes)=>{
        let ResA: any = dataRes;
        //console.log(ResA);
        this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) TotDocApr","","","docreqcon","WHERE NRegCon="+Dat.NRegCon+" AND Estatus=|Aprobado|","","",true).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(ResB);
          //console.log("DR:"+ResA.data[0].TotDocReq+">>>"+"DA:"+ResB.data[0].TotDocApr)
          if (parseInt(ResA.data[0].TotDocReq) == parseInt(ResB.data[0].TotDocApr)){
            //console.log("Req Com")
            this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Activo|","usuarios","WHERE NRegistro="+Dat.NRegCon+"","","",true).then((dataRes)=>{
              let Res: any = dataRes;
              //console.log(Res);
            }).catch((err)=>{console.log(err)});
          }else{
            //console.log("Req Inc")
          }
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }
  RecDoc(Dat){
    this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Rechazado|","docreqcon","WHERE NRegistro="+Dat.NRegistro,"","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("El Documento fue Rechazado","Peligro");
      this.CarLis();
    }).catch((err)=>{console.log(err)});
  }

  Documentos(Dat){
    this.DetDat = Dat;
    //this.CarDoc(this.DetDat.NRegistro);
  }

  /*
  openDialog() {
    const dialogRef = this.dialog.open("");

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  */


  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          
          //console.log(dataRes);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }
}