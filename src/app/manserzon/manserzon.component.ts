import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

declare var google: any;
declare var PolygonCreator: any;

@Component({
  selector: 'app-manserzon',
  templateUrl: './manserzon.component.html',
  styleUrls: ['./manserzon.component.scss']
})
export class ManserzonComponent implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  map: any;
  creator_geo: any;
  PunGeo: any = [];
  Geo0: any;

  DatRes: any;
  DonBus: string = "";
  DetDat: any;

  DGM: any = {
    "Nombre":"Zonas",
    "CamMod":[
      {"Tit":"Estado","Cmp":"NRegTar","Tip":"SelectQry","Req":"Si","VD":"","Val":"","Blo":"No","Qry":{"Cmps":"NRegistro,Nombre","Tab":"tarifas","Don":"WHERE Estatus = |Activa|","Ord":"ORDER BY Nombre","Res":[]}},
      {"Tit":"Nombre","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"Inactiva","Val":"","Blo":"Si","Opcs":[{"Val":"Activa","Tex":"Activa"},{"Val":"Inactiva","Tex":"Inactiva"}]},
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"zonas",
    "Donde":"WHERE NRegistro > 0",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2],"Col":[1,2]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios, public location: Location) {
  }

  ngOnInit() {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }

    this.CarCmpQry();
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  Geocerca(Dat){
    this.DetDat = Dat;

    this.showMap(
      function(thiss){
        thiss.MosGeoMap(thiss.DetDat);
        //console.log("Hola");
      }
    );

  }

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          
          //console.log(dataRes);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.servicios.VPB = 100;
      }
    );
    */
  }

  showMap(callback){
    var location;
    
    if (this.servicios.UsuMat.UbiAct != ""){
      var ULLM = this.servicios.UsuMat.UbiAct.split(",");
      location = new google.maps.LatLng(parseFloat(ULLM[0]),parseFloat(ULLM[1]));
      console.log(parseFloat(ULLM[0])+"/"+parseFloat(ULLM[1]))
    }else{
      //location = new google.maps.LatLng(-17.7655264,-63.226613);
      location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      styles: [
        /*
        {
          featureType: 'poi',
          stylers: [{visibility: 'off'}]
        },
        {
          featureType: 'transit.station',
          stylers: [{visibility: 'off'}]
        }
        */
      ]
    }

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    //const trafficLayer = new google.maps.TrafficLayer();
    //trafficLayer.setMap(this.map);
    callback(this);
  }

  MosGeoMap(Dat){
    if (Dat.Puntos != ""){
      var limits = new google.maps.LatLngBounds();
      var LatsLons = Dat.Puntos.split("!");
      var MLatLon = []
      
      this.PunGeo.length = 0;
      for(var n = 0; n < LatsLons.length; n++){
        LatsLons[n] = LatsLons[n].replace("(","");
        LatsLons[n] = LatsLons[n].replace(")","");
        LatsLons[n] = LatsLons[n].replace(" ","");
        
        MLatLon = LatsLons[n].split(",");
        this.PunGeo.push (new google.maps.LatLng(MLatLon[0],MLatLon[1]));
        limits.extend(new google.maps.LatLng(MLatLon[0],MLatLon[1]));
      }
      MLatLon = LatsLons[0].split(",");
      this.PunGeo.push (new google.maps.LatLng(MLatLon[0],MLatLon[1]));
          
      this.Geo0 = new google.maps.Polygon({
        paths: this.PunGeo,
        strokeColor: '#0000ff',
        strokeOpacity: 0.7,
        strokeWeight: 3,
        fillColor: '#aaaaff',
        fillOpacity: 0.1,
        editable:true
      });
      
      this.Geo0.setMap(this.map);
      setTimeout(() => {
        this.map.fitBounds(limits);
      }, 350);
    }else{
      this.creator_geo = new PolygonCreator(this.map);
    }
  }

  GuaGeo(Dat){
    var Pts = "";

    if (Dat.Puntos != ""){
      var paths = this.Geo0.getPaths();
      var path;
      for (var p = 0; p < paths.getLength(); p++) {
          path = paths.getAt(p);
          for (var i = 0; i < path.getLength(); i++) {
              Pts = Pts + path.getAt(i);
              if (i < (path.getLength()-1)){Pts = Pts + "!"}
          }
      }
    }else{
      Pts = this.creator_geo.showData();
      if (Pts == null){this.servicios.EnvMsgSim("Primero debe crear la geocerca","Advertencia"); return;}
      let re = /\)\(/gi;
      Pts = Pts.replace(re,")!(");
    }

    this.servicios.AccSobBDAA("UPDATE","No","","","Puntos = |"+Pts+"|, Estatus = |Activa|","zonas","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
      this.CarLis();
      
      this.servicios.EnvMsgSim("Los cambios fueron guardados","Normal");
      this.closeAddExpenseModal.nativeElement.click();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("UPDATE","No","","","Puntos = |"+Pts+"|, Estatus = |Activa|","zonas","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.CarLis();
        
        thiss.servicios.EnvMsgSim("Los cambios fueron guardados","Normal");
        thiss.closeAddExpenseModal.nativeElement.click();
      }
    );
    */
  }


  ResetGeo(){
    this.creator_geo.destroy();
    this.creator_geo = null;
    this.creator_geo = new PolygonCreator(this.map);
  }

  Volver(){
    this.location.back();
  }

}