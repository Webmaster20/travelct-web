import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManserzonComponent } from './manserzon.component';

describe('ManserzonComponent', () => {
  let component: ManserzonComponent;
  let fixture: ComponentFixture<ManserzonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManserzonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManserzonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
