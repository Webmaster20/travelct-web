import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Servicios } from '../../servicios/servicios';
import { Router } from '@angular/router';

declare var google: any;
declare var PolygonCreator: any;

@Component({
  selector: 'app-mansertar',
  templateUrl: './mansertar.component.html',
  styleUrls: ['./mansertar.component.scss']
})
export class MansertarComponent implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  DatRes: any;
  DonBus: string = "";
  DetDat: any;
  OJE: any = [];

  LisZon: any;
  DetTar: any;
  ZonaA: string = "";
  Sentido: string = "";
  ZonaB: string = "";
  Precio: number;
  Estatus: string = "";
  NRegDetTarSel: number = 0;

  DiaFer: any;
  DetFer: any;
  Fecha: string = "";
  Descripcion: string = "";

  HorEspIni: string = "";
  HorEspFin: string = "";

  map: any;
  creator_geo: any;
  PunGeo: any = [];
  Geo0: any;


  DGM: any = {
    "Nombre":"Tarifas",
    "CamMod":[
      {"Tit":"Estado","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      //{"Tit":"Descripción","Cmp":"Descripcion","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Kilometro Base","Cmp":"RecBasKil","Tip":"Entero","Req":"Si","VD":"","Val":""},
      {"Tit":"Precio Base $","Cmp":"RecBasPre","Tip":"Numero","Req":"Si","VD":"","Val":""},
      {"Tit":"Precio Kilometro Adicional $","Cmp":"RecAdiPre","Tip":"Numero","Req":"Si","VD":"","Val":""},
      //{"Tit":"Porcentaje Valor Productos %","Cmp":"PorValPro","Tip":"Numero","Req":"Si","VD":"","Val":""},
      //{"Tit":"Precio Fijo Servicio $","Cmp":"PreFijSer","Tip":"Numero","Req":"Si","VD":"","Val":""},
      {"Tit":"Precio Minuto $","Cmp":"EspMinPre","Tip":"Numero","Req":"No","VD":"0","Val":""},
      //{"Tit":"Precio Dia Feriado $","Cmp":"DiaFerPre","Tip":"Numero","Req":"No","VD":"0","Val":""},
      //{"Tit":"Precio Horario Especial $","Cmp":"HorEspPre","Tip":"Numero","Req":"No","VD":"0","Val":""},
      //{"Tit":"Precio Pasajero Adicional $","Cmp":"PerAdiPre","Tip":"Numero","Req":"Si","VD":"","Val":""},
      //{"Tit":"Tarifa Dinámica %","Cmp":"TarDin","Tip":"Numero","Req":"No","VD":"0","Val":""},
      {"Tit":"Ganancia Servicio %","Cmp":"Ganancia","Tip":"Numero","Req":"Si","VD":"","Val":""},
      //{"Tit":"Ganancia Productos %","Cmp":"GananciaPro","Tip":"Numero","Req":"Si","VD":"","Val":""},
      //{"Tit":"Multa por Cancelación $","Cmp":"MulCan","Tip":"Numero","Req":"No","VD":"0","Val":""},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activa","Tex":"Activa"},{"Val":"Inactiva","Tex":"Inactiva"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"tarifas",
    "Donde":"",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2,3,4,5],"Col":[1,2,3]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios) {
  }

  ngOnInit() {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarCmpQry();
    this.CarLis();
    //this.CarLisZon();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
    //this.CarLisZon();
  }

  Zonas(Dat){
    this.DetDat = Dat;
    this.CarLisDetTar(Dat);
    this.CarLisZon(Dat);
  }
  
  GuaTarZon(Dat){
    if (!this.servicios.ValCam("ZonaA","Zona A","Texto",this.ZonaA,"Si")){return;}
    if (!this.servicios.ValCam("Sentido","Sentido","Texto",this.Sentido,"Si")){return;}
    if (!this.servicios.ValCam("ZonaB","Zona B","Texto",this.ZonaB,"Si")){return;}
    if (!this.servicios.ValCam("Precio","Porcentaje","Porcentaje",this.Precio,"Si")){return;}
    if (!this.servicios.ValCam("Estatus","Estado","Texto",this.Estatus,"Si")){return;}

    if (this.NRegDetTarSel == 0){
      this.servicios.AccSobBDAA("INSERT","No","NRegTar,NRegZonA,Sentido,NRegZonB,Precio,Estatus",Dat.NRegistro+","+this.ZonaA+",|"+this.Sentido+"|,"+this.ZonaB+","+this.Precio+",|"+this.Estatus+"|","","tarifaszonas","","","",false).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.CarLisDetTar(Dat);
      }).catch((err)=>{console.log(err)});

      /*
      this.servicios.AccSobBD("INSERT","No","NRegTar,NRegZonA,Sentido,NRegZonB,Precio,Estatus",Dat.NRegistro+","+this.ZonaA+",|"+this.Sentido+"|,"+this.ZonaB+","+this.Precio+",|"+this.Estatus+"|","","tarifaszonas","","","","Ins",this,
        function(dataRes,thiss){
          //console.log(dataRes);
          thiss.CarLisDetTar(Dat);
        }
      );
      */
    }else{
      this.servicios.AccSobBDAA("UPDATE","No","","","NRegZonA="+this.ZonaA+", Sentido=|"+this.Sentido+"|, NRegZonB="+this.ZonaB+", Precio="+this.Precio+", Estatus=|"+this.Estatus+"|","tarifaszonas","WHERE NRegistro="+this.NRegDetTarSel,"","",false).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.CarLisDetTar(Dat);
      }).catch((err)=>{console.log(err)});

      /*
      this.servicios.AccSobBD("UPDATE","No","","","NRegZonA="+this.ZonaA+", Sentido=|"+this.Sentido+"|, NRegZonB="+this.ZonaB+", Precio="+this.Precio+", Estatus=|"+this.Estatus+"|","tarifaszonas","WHERE NRegistro="+this.NRegDetTarSel,"","","Upd",this,
        function(dataRes,thiss){
          //console.log(dataRes);
          thiss.CarLisDetTar(Dat);
        }
      );
      */
    }

    this.ZonaA = "";
    this.Sentido = "";
    this.ZonaB = "";
    this.Precio = null;
    this.Estatus = "";
    
    this.NRegDetTarSel = 0;
  }

  ModDetTar(det){
    this.NRegDetTarSel = det.NRegistro;

    this.ZonaA = det.NRegZonA;
    this.Sentido = det.Sentido;
    this.ZonaB = det.NRegZonB;
    this.Precio = det.Precio;
    this.Estatus = det.Estatus;
  }

  EliDetTar(Dat,det){
    this.servicios.AccSobBDAA("DELETE","No","","","","tarifaszonas","WHERE NRegistro="+det.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.CarLisDetTar(Dat);
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("DELETE","No","","","","tarifaszonas","WHERE NRegistro="+det.NRegistro,"","","Del",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.CarLisDetTar(Dat);
      }
    );
    */
  }
  
  DefZon(){
    this.router.navigate(["home/manserzon"]);
  }

  Feriados(Dat){
    this.DetDat = Dat;
    this.DiaFer = Dat.DiaFer.split(",");
    this.HorEspIni = Dat.HorEspIni;
    this.HorEspFin = Dat.HorEspFin;
    this.CarLisDetFer(Dat);
  }
  
  GuaTarFer(Dat){
    let StrDiaFer = this.DiaFer.toString();
    this.servicios.AccSobBDAA("UPDATE","No","","","DiaFer=|"+StrDiaFer+"|","tarifas","WHERE NRegistro="+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
      this.CarLis();
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("UPDATE","No","","","DiaFer=|"+StrDiaFer+"|","tarifas","WHERE NRegistro="+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
        thiss.CarLis();
      }
    );
    */
  }

  GuaHorEsp(Dat){
    this.servicios.AccSobBDAA("UPDATE","No","","","HorEspIni=|"+this.HorEspIni+"|, HorEspFin=|"+this.HorEspFin+"|","tarifas","WHERE NRegistro="+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
      this.CarLis();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("UPDATE","No","","","HorEspIni=|"+this.HorEspIni+"|, HorEspFin=|"+this.HorEspFin+"|","tarifas","WHERE NRegistro="+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
        thiss.CarLis();
      }
    );
    */
  }

  AgrDetFer(Dat){
    if (!this.servicios.ValCam("Fecha","Fecha","Texto",this.Fecha,"Si")){return;}
    if (!this.servicios.ValCam("Descripcion","Descripcion","Texto",this.Descripcion,"Si")){return;}

    this.servicios.AccSobBDAA("INSERT","No","NRegTar,Fecha,Descripcion",Dat.NRegistro+",|"+this.Fecha+"|,|"+this.Descripcion+"|","","tarifasferiados","","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.CarLisDetFer(Dat);
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("INSERT","No","NRegTar,Fecha,Descripcion",Dat.NRegistro+",|"+this.Fecha+"|,|"+this.Descripcion+"|","","tarifasferiados","","","","Ins",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.CarLisDetFer(Dat);
      }
    );
    */

    this.Fecha = "";
    this.Descripcion = "";
  }

  EliDetFer(Dat,det){
    this.servicios.AccSobBDAA("DELETE","No","","","","tarifasferiados","WHERE NRegistro = "+det.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.CarLisDetFer(Dat);
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("DELETE","No","","","","tarifasferiados","WHERE NRegistro = "+det.NRegistro,"","","Del",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.CarLisDetFer(Dat);
      }
    );
    */

    this.Fecha = "";
    this.Descripcion = "";
  }









  Geocerca(Dat){
    this.DetDat = Dat;

    this.showMap(
      function(thiss){
        thiss.MosGeoMap(thiss.DetDat);
        //console.log("Hola");
      }
    );

  }

  showMap(callback){
    var location;
    
    if (this.servicios.UsuMat.UbiAct != ""){
      var ULLM = this.servicios.UsuMat.UbiAct.split(",");
      location = new google.maps.LatLng(parseFloat(ULLM[0]),parseFloat(ULLM[1]));
      console.log(parseFloat(ULLM[0])+"/"+parseFloat(ULLM[1]))
    }else{
      //location = new google.maps.LatLng(-17.7655264,-63.226613);
      location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      styles: [
        /*
        {
          featureType: 'poi',
          stylers: [{visibility: 'off'}]
        },
        {
          featureType: 'transit.station',
          stylers: [{visibility: 'off'}]
        }
        */
      ]
    }

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    //const trafficLayer = new google.maps.TrafficLayer();
    //trafficLayer.setMap(this.map);
    callback(this);
  }

  MosGeoMap(Dat){
    if (Dat.Puntos != ""){
      var limits = new google.maps.LatLngBounds();
      var LatsLons = Dat.Puntos.split("!");
      var MLatLon = []
      
      this.PunGeo.length = 0;
      for(var n = 0; n < LatsLons.length; n++){
        LatsLons[n] = LatsLons[n].replace("(","");
        LatsLons[n] = LatsLons[n].replace(")","");
        LatsLons[n] = LatsLons[n].replace(" ","");
        
        MLatLon = LatsLons[n].split(",");
        this.PunGeo.push (new google.maps.LatLng(MLatLon[0],MLatLon[1]));
        limits.extend(new google.maps.LatLng(MLatLon[0],MLatLon[1]));
      }
      MLatLon = LatsLons[0].split(",");
      this.PunGeo.push (new google.maps.LatLng(MLatLon[0],MLatLon[1]));
          
      this.Geo0 = new google.maps.Polygon({
        paths: this.PunGeo,
        strokeColor: '#0000ff',
        strokeOpacity: 0.7,
        strokeWeight: 3,
        fillColor: '#aaaaff',
        fillOpacity: 0.1,
        editable:true
      });
      
      this.Geo0.setMap(this.map);
      setTimeout(() => {
        this.map.fitBounds(limits);
      }, 350);
    }else{
      this.creator_geo = new PolygonCreator(this.map);
    }
  }

  GuaGeo(Dat){
    var Pts = "";

    if (Dat.Puntos != ""){
      var paths = this.Geo0.getPaths();
      var path;
      for (var p = 0; p < paths.getLength(); p++) {
          path = paths.getAt(p);
          for (var i = 0; i < path.getLength(); i++) {
              Pts = Pts + path.getAt(i);
              if (i < (path.getLength()-1)){Pts = Pts + "!"}
          }
      }
    }else{
      Pts = this.creator_geo.showData();
      if (Pts == null){this.servicios.EnvMsgSim("Primero debe crear la geocerca","Advertencia"); return;}
      let re = /\)\(/gi;
      Pts = Pts.replace(re,")!(");
    }

    this.servicios.AccSobBDAA("UPDATE","No","","","Puntos = |"+Pts+"|, Estatus = |Activa|","tarifas","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
      this.CarLis();
      
      this.servicios.EnvMsgSim("Los cambios fueron guardados","Normal");
      this.closeAddExpenseModal.nativeElement.click();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("UPDATE","No","","","Puntos = |"+Pts+"|, Estatus = |Activa|","tarifas","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.CarLis();
        
        thiss.servicios.EnvMsgSim("Los cambios fueron guardados","Normal");
        thiss.closeAddExpenseModal.nativeElement.click();
      }
    );
    */
  }


  ResetGeo(){
    this.creator_geo.destroy();
    this.creator_geo = null;
    this.creator_geo = new PolygonCreator(this.map);
  }


  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",false).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});

        /*
        this.servicios.AccSobBD("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"","CarVar",this,
          function(dataRes,thiss){
            //console.log(dataRes);
            thiss.DGM.CamMod[IDC].Qry.Res = dataRes.data;
          }
        );
        */
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.servicios.VPB = 100;
        thiss.CarLisExp();
      }
    );
    */
  }

  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }

  CarLisZon(Dat){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","zonas","WHERE NRegTar = "+Dat.NRegistro+" AND Estatus = |Activa|","ORDER BY Nombre","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisZon = Res.data;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","zonas","WHERE Estatus = |Activa|","ORDER BY Nombre","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.LisZon = dataRes.data;
      }
    );
    */
  }

  CarLisDetTar(Dat){
    this.servicios.AccSobBDAA("SELECT","Si","*, (SELECT Nombre FROM zonas WHERE NRegistro = A.NRegZonA) DesZonA, (SELECT Nombre FROM zonas WHERE NRegistro = A.NRegZonB) DesZonB","","","tarifaszonas A","WHERE NRegTar = "+Dat.NRegistro,"ORDER BY NRegistro","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DetTar = Res.data;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*, (SELECT Nombre FROM zonas WHERE NRegistro = A.NRegZonA) DesZonA, (SELECT Nombre FROM zonas WHERE NRegistro = A.NRegZonB) DesZonB","","","tarifaszonas A","WHERE NRegTar = "+Dat.NRegistro,"ORDER BY NRegistro","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DetTar = dataRes.data;
      }
    );
    */
  }

  CarLisDetFer(Dat){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","tarifasferiados","WHERE NRegTar = "+Dat.NRegistro,"ORDER BY NRegistro","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DetFer = Res.data;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","tarifasferiados","WHERE NRegTar = "+Dat.NRegistro,"ORDER BY NRegistro","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DetFer = dataRes.data;
      }
    );
    */
  }

}
