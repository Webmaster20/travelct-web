import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MansertarComponent } from './mansertar.component';

describe('MansertarComponent', () => {
  let component: MansertarComponent;
  let fixture: ComponentFixture<MansertarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MansertarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MansertarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
