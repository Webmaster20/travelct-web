import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-ini',
  templateUrl: './ini.component.html',
  styleUrls: ['./ini.component.scss']
})
export class IniComponent implements OnInit {

  constructor(public router: Router,public servicios: Servicios) { }

  ngOnInit(): void {
    this.router.navigate(["login"]);
  }
}
