import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-billetera',
  templateUrl: './billetera.component.html',
  styleUrls: ['./billetera.component.scss']
})
export class BilleteraComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";

  FAD = new Date();
  FecAnt: string = "";
  FecAct: string = new Date().toISOString();

  FecDes: string = this.FecAct.substr(0,10);
  FecHas: string = this.FecAct.substr(0,10);

  TotAbo: number = 0;
  TotRet: number = 0;
  TotCob: number = 0;
  TotGan: number = 0;

  AboMin: number = 0;

  DetDat: any;
  Trans: any;

  OJE: any = [];

  DGM: any = {
    "Nombre":"Billetera",
    "CamMod":[
      {"Tit":"Nombre","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Apellido","Cmp":"Apellido","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Telefono","Cmp":"Telefono","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Tipo","Cmp":"Tipo","Tip":"Select","Req":"No","VD":"","Val":"","Opcs":[{"Val":"Vendedor","Tex":"Vendedor"},{"Val":"Repartidor","Tex":"Repartidor"}]},
      {"Tit":"Email","Cmp":"Email","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Saldo","Cmp":"Saldo","Tip":"Numero","Req":"No","VD":"","Val":""},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activo","Tex":"Activo"},{"Val":"Nuevo","Tex":"Nuevo"},{"Val":"Bloqueado","Tex":"Bloqueado"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"usuarios",
    "Donde":"WHERE Tipo IN (|Vendedor|,|Empresa|,|Repartidor|)",
    "Ordena":"ORDER BY NRegistro DESC",
    "TabFor":{"Fil":[1,2,3],"Col":[1,2]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios) {
  }

  ngOnInit(){

    this.FAD.setMonth(this.FAD.getMonth() - 1);
    this.FecAnt = this.FAD.toISOString();
    this.FecDes = this.FecAnt.substr(0,10);

    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }

    this.CalTotAbo();
    this.CalTotRet();
    this.CalTotCob();
    this.CalTotGan();
    this.CarTra();
    //this.CarOpc();

  }
  ngAfterViewInit(){
    
  }

  ActBil(){
    this.CalTotAbo();
    this.CalTotRet();
    this.CalTotCob();
    this.CalTotGan();
    this.CarTra();
  }

  VerSolAbo(){
    this.router.navigate(["home/solabo"]);
  }

  VerSolRet(){
    this.router.navigate(["home/solret"]);
  }

  VerPorCob(){
    window.localStorage.setItem("buscarConductores","Saldo { 0");
    this.router.navigate(["home/manusucon"]);
  }

  CalTotAbo(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotAbo","","","billetera","WHERE Transaccion = |Abono| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotAbo = Res.data[0].TotAbo;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","SUM(Monto) TotAbo","","","billetera","WHERE Transaccion = |Abono| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.TotAbo = dataRes.data[0].TotAbo;
        thiss.servicios.VPB = 25;
      }
    );
    */
  }

  CalTotRet(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotRet","","","billetera","WHERE Transaccion = |Retiro| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotRet = Res.data[0].TotRet;
    }).catch((err)=>{console.log(err)});
    
    /*
    this.servicios.AccSobBD("SELECT","Si","SUM(Monto) TotRet","","","billetera","WHERE Transaccion = |Retiro| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.TotRet = dataRes.data[0].TotRet;
        thiss.servicios.VPB = 50;
      }
    );
    */
  }

  CalTotCob(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Saldo) TotCob","","","usuarios","WHERE Saldo { 0 AND Estatus = |Activo|","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotCob = Res.data[0].TotCob;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","SUM(Saldo) TotCob","","","usuarios","WHERE Saldo { 0 AND Estatus = |Activo|","","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.TotCob = dataRes.data[0].TotCob;
        thiss.servicios.VPB = 75;
      }
    );
    */
  }

  CalTotGan(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotGan","","","billetera","WHERE Transaccion = |Cobro Ganancia| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotGan = Res.data[0].TotGan;
    }).catch((err)=>{console.log(err)});
    
    /*
    this.servicios.AccSobBD("SELECT","Si","SUM(Monto) TotGan","","","billetera","WHERE Transaccion = |Cobro Ganancia| AND FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.TotGan = dataRes.data[0].TotGan;
        thiss.servicios.VPB = 100;
      }
    );
    */
  }

  CarTra(){
    this.servicios.AccSobBDAA("SELECT","Si","*, (SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu) NomUsu, (SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu) TipUsu","","","billetera A","WHERE FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","ORDER BY FecHor DESC","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Trans = Res.data;
      this.DatRes = Res.data;

      this.CarTraExp();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*, (SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu) NomUsu, (SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu) TipUsu","","","billetera A","WHERE FecHor BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","ORDER BY FecHor DESC","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.Trans = dataRes.data;
        thiss.DatRes = dataRes.data;
        thiss.servicios.VPB = 100;
        thiss.CarTraExp();
      }
    );
    */
  }
  CarTraExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      
      BOJE["Usuario"] = this.DatRes[n].NomUsu;
      BOJE["Tipo"] = this.DatRes[n].TipUsu;
      BOJE["Transaccion"] = this.DatRes[n].Transaccion;
      BOJE["Descripcion"] = this.DatRes[n].Descripcion;
      BOJE["Monto"] = this.servicios.NumFor(this.DatRes[n].Monto,2);
      BOJE["Fecha Hora"] = this.servicios.ForFecHor(this.DatRes[n].FecHor);

      this.OJE.push(BOJE);
    }
  }

  CarOpc(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","opciones","WHERE NRegistro = 1","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.AboMin = Res.data[0].BilMonMinAbo;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","opciones","WHERE NRegistro = 1","","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.AboMin = dataRes.data[0].BilMonMinAbo;
      }
    );
    */
  }

  GuaOpc(){
    if (!this.servicios.ValCam("AboMin","Monto mínimo de Abono","Numero",this.AboMin,"Si")){return;}

    this.servicios.AccSobBDAA("UPDATE","No","","","BilMonMinAbo="+this.AboMin+"","opciones","WHERE NRegistro=1","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("Las opciones fueron guardadas","Informacion");
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("UPDATE","No","","","BilMonMinAbo="+this.AboMin+"","opciones","WHERE NRegistro=1","","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.EnvMsgSim("Las opciones fueron guardadas","Informacion");
      }
    );
    */
  }

}