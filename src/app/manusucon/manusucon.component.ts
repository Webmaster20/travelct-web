import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-manusucon',
  templateUrl: './manusucon.component.html',
  styleUrls: ['./manusucon.component.scss']
})
export class ManusuconComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  DetDat: any;
  Trans: any;
  Vehic: any;

  Tipo: string = "";
  Marca: string = "";
  Modelo: string = "";
  Matricula: string = "";
  Ano: string = "";
  Estatus: string = "";

  NRegDetVehSel:number = 0;

  DGM: any = {
    "Nombre":"Conductores",
    "CamMod":[
      {"Tit":"Foto Perfil","Cmp":"FotPer","Tip":"Imagen","Req":"Si","VD":"fpdu.png","Val":"","Est":"","Car":"usu"},
      {"Tit":"Nombre","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Apellido","Cmp":"Apellido","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Teléfono","Cmp":"Telefono","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Email","Cmp":"Email","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Password","Cmp":"Password","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Saldo","Cmp":"Saldo","Tip":"Wallet","Req":"Si","VD":"","Val":""},
      {"Tit":"Tipo","Cmp":"Tipo","Tip":"Texto","Req":"Si","VD":"Conductor","Val":"","Blo":"Si"},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activo","Tex":"Activo"},{"Val":"Bloqueado","Tex":"Bloqueado"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"usuarios",
    "Donde":"WHERE Tipo = |Conductor|",
    "Ordena":"ORDER BY NRegistro DESC",
    "TabFor":{"Fil":[1,2,3,4],"Col":[1,2,3]},
    "Carpeta":"usu"
  };

  constructor(public router: Router, public servicios: Servicios) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  Detalles(Dat){
    this.DetDat = Dat;
    this.CarTra(this.DetDat.NRegistro);
  }

  Vehiculos(Dat){
    this.DetDat = Dat;
    this.CarVeh(this.DetDat.NRegistro);
  }

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }

  CarTra(NRegUsu){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","billetera","WHERE NRegUsu = "+NRegUsu,"ORDER BY FecHor DESC","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Trans = Res.data;
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","billetera","WHERE NRegUsu = "+NRegUsu,"ORDER BY FecHor DESC","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.Trans = dataRes.data;
      }
    );
    */
  }

  CarVeh(NRegUsu){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","vehiculos","WHERE NRegUsu = "+NRegUsu,"ORDER BY Marca","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Vehic = Res.data;
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","repveh","WHERE NRegRep = "+NRegUsu,"ORDER BY Marca","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.Vehic = dataRes.data;
      }
    );
    */
  }


  GuaVeh(Dat){
    if (!this.servicios.ValCam("Tipo","Tipo","Texto",this.Tipo,"Si")){return;}
    if (!this.servicios.ValCam("Marca","Marca","Texto",this.Marca,"Si")){return;}
    if (!this.servicios.ValCam("Modelo","Modelo","Texto",this.Modelo,"Si")){return;}
    if (!this.servicios.ValCam("Matricula","Matricula","Texto",this.Matricula,"Si")){return;}
    if (!this.servicios.ValCam("Ano","Año","Texto",this.Ano,"Si")){return;}
    if (!this.servicios.ValCam("Estatus","Estado","Texto",this.Estatus,"Si")){return;}

    if (this.NRegDetVehSel == 0){
      this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Tipo,Marca,Modelo,Matricula,Ano,Estatus",Dat.NRegistro+",|"+this.Tipo+"|,|"+this.Marca+"|,|"+this.Modelo+"|,|"+this.Matricula+"|,|"+this.Ano+"|,|"+this.Estatus+"|","","vehiculos","","","",false).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.CarVeh(Dat.NRegistro);
      }).catch((err)=>{console.log(err)});
      /*
      this.servicios.AccSobBD("INSERT","No","NRegRep,Tipo,Marca,Modelo,Matricula,Ano,Estatus",Dat.NRegistro+",|"+this.Tipo+"|,|"+this.Marca+"|,|"+this.Modelo+"|,|"+this.Matricula+"|,|"+this.Ano+"|,|"+this.Estatus+"|","","repveh","","","","Ins",this,
        function(dataRes,thiss){
          //console.log(dataRes);
          thiss.CarVeh(Dat.NRegistro);
        }
      );
      */
    }else{
      this.servicios.AccSobBDAA("UPDATE","No","","","Tipo=|"+this.Tipo+"|, Marca=|"+this.Marca+"|, Modelo=|"+this.Modelo+"|, Matricula=|"+this.Matricula+"|, Ano=|"+this.Ano+"|, Estatus=|"+this.Estatus+"|","vehiculos","WHERE NRegistro="+this.NRegDetVehSel,"","",false).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.CarVeh(Dat.NRegistro);
      }).catch((err)=>{console.log(err)});
      /*
      this.servicios.AccSobBD("UPDATE","No","","","Tipo=|"+this.Tipo+"|, Marca=|"+this.Marca+"|, Modelo=|"+this.Modelo+"|, Matricula=|"+this.Matricula+"|, Ano=|"+this.Ano+"|, Estatus=|"+this.Estatus+"|","repveh","WHERE NRegistro="+this.NRegDetVehSel,"","","Upd",this,
        function(dataRes,thiss){
          //console.log(dataRes);
          thiss.CarVeh(Dat.NRegistro);
        }
      );
      */
    }

    this.Tipo = "";
    this.Marca = "";
    this.Modelo = "";
    this.Matricula = "";
    this.Ano = "";
    this.Estatus = "";
    
    this.NRegDetVehSel = 0;
  }

  ModDetVeh(det){
    this.NRegDetVehSel = det.NRegistro;

    this.Tipo = det.Tipo;
    this.Marca = det.Marca;
    this.Modelo = det.Modelo;
    this.Matricula = det.Matricula;
    this.Ano = det.Ano;
    this.Estatus = det.Estatus;
  }

  EliDetVeh(Dat,det){
    this.servicios.AccSobBDAA("DELETE","No","","","","vehiculos","WHERE NRegistro="+det.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.CarVeh(Dat.NRegistro);
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("DELETE","No","","","","repveh","WHERE NRegistro="+det.NRegistro,"","","Del",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.CarVeh(Dat.NRegistro);
      }
    );
    */
  }
}