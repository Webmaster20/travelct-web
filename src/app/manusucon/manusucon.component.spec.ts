import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManusuconComponent } from './manusucon.component';

describe('ManusuconComponent', () => {
  let component: ManusuconComponent;
  let fixture: ComponentFixture<ManusuconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManusuconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManusuconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
