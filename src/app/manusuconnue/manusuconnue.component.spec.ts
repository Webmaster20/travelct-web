import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManusuconnueComponent } from './manusuconnue.component';

describe('ManusuconnueComponent', () => {
  let component: ManusuconnueComponent;
  let fixture: ComponentFixture<ManusuconnueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManusuconnueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManusuconnueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
