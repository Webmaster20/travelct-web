import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-solret',
  templateUrl: './solret.component.html',
  styleUrls: ['./solret.component.scss']
})
export class SolretComponent implements OnInit {

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;
  @ViewChild('closeAddExpenseModal2') closeAddExpenseModal2: ElementRef;

  DatRes: any;
  DonBus: string = "";
  DetDat: any;
  LisCue: any;
  CueUsu: string;
  Razon: string;

  Tipo: string = "";
  REF: string = "";
  Fecha: string = "";

  DGM: any = {
    "Nombre":"Solicitud de retiro",
    "CamMod":[
      {"Tit":"Usuario","Cmp":"DesUsu","Tip":"Texto","Req":"Si","VD":"","Val":"","SubQry":"(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Tipo","Cmp":"TipUsu","Tip":"Texto","Req":"Si","VD":"","Val":"","SubQry":"(SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Saldo","Cmp":"SalUsu","Tip":"Numero","Req":"Si","VD":"","Val":"","SubQry":"(SELECT Saldo FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Descripción","Cmp":"Descripcion","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Monto","Cmp":"Monto","Tip":"Numero","Req":"No","VD":"","Val":""},
      {"Tit":"Solicitado","Cmp":"FecHorReg","Tip":"FechaHora","Req":"No","VD":"","Val":""}
    ],
    "OpcFil":"Si",
    "CamQry":"*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu) DesUsu, (SELECT Telefono FROM usuarios WHERE NRegistro = A.NRegUsu) TelUsu, (SELECT Email FROM usuarios WHERE NRegistro = A.NRegUsu) EmaUsu, (SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegUsu) FotUsu, (SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu) TipUsu, (SELECT Saldo FROM usuarios WHERE NRegistro = A.NRegUsu) SalUsu",
    "Tabla":"solret A",
    "Donde":"WHERE Estatus = |Nueva|",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2,3],"Col":[1,2]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios, public location: Location) {
  }

  ngOnInit() {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  Detalles(Dat){
    this.DetDat = Dat;
    this.CarLisCue(Dat.NRegUsu);
  }


  Aceptar(Dat){
    if (parseFloat(Dat.SalUsu) < parseFloat(Dat.Monto)){this.servicios.EnvMsgSim("El monto solicitado no puede ser superior al saldo","Advertencia"); return;}

    if (!this.servicios.ValCam("CueUsu","Cuenta del usuario","Texto",this.CueUsu,"Si")){return;}
    if (!this.servicios.ValCam("Tipo","Tipo","Texto",this.Tipo,"Si")){return;}
    if (!this.servicios.ValCam("REF","Número de referencia","Texto",this.REF,"Si")){return;}
    if (!this.servicios.ValCam("Fecha","Fecha de transacción","Texto",this.Fecha,"Si")){return;}

    let Descripcion = "Retiro de fondos "+this.Tipo+" REF:"+this.REF+" el "+this.servicios.ForFec(this.Fecha)+" en cuenta "+this.CueUsu;
    //console.log(Descripcion);

    this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Transaccion,Descripcion,Monto,FecHor","|"+Dat.NRegUsu+"|,|Retiro|,|"+Descripcion+"|,("+Dat.Monto+"*-1),NOW()","","billetera","","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.AccSobBDAA("UPDATE","No","","","Saldo = Saldo - "+Dat.Monto,"usuarios","WHERE NRegistro = "+Dat.NRegUsu,"","",false).then((dataRes)=>{
        let ResB: any = dataRes;
        //console.log(Res);
        this.servicios.AccSobBDAA("UPDATE","No","","","Estatus = |Aceptada|, FecHorRes=NOW(), Tipo=|"+this.Tipo+"|, Referencia=|"+this.REF+"|, Fecha=|"+this.Fecha+"|","solret","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
          let ResC: any = dataRes;
          //console.log(ResC);
          this.DatRes = ResC.data;
          this.CarLis();
          this.closeAddExpenseModal.nativeElement.click();
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD("INSERT","No","NRegUsu,Transaccion,Descripcion,Monto,FecHor","|"+Dat.NRegUsu+"|,|Retiro|,|"+Descripcion+"|,("+Dat.Monto+"*-1),NOW()","","billetera","","","","Ins",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.AccSobBD("UPDATE","No","","","Saldo = Saldo - "+Dat.Monto,"usuarios","WHERE NRegistro = "+Dat.NRegUsu,"","","Upd",thiss,
          function(dataRes,thiss){
            //console.log(dataRes);
            thiss.servicios.AccSobBD("UPDATE","No","","","Estatus = |Aceptada|, FecHorRes=NOW(), Tipo=|"+thiss.Tipo+"|, Referencia=|"+thiss.REF+"|, Fecha=|"+thiss.Fecha+"|","solret","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",thiss,
              function(dataRes,thiss){
                //console.log(dataRes);
                thiss.DatRes = dataRes.data;
                thiss.CarLis();
                thiss.closeAddExpenseModal.nativeElement.click();
              }
            );
          }
        );
      }
    );
    */
  }

  Rechazar(Dat){
    if (!this.servicios.ValCam("Razon","Razón del rechazo","Texto",this.Razon,"Si")){return;}

    this.servicios.AccSobBDAA("UPDATE","No","","","Estatus = |Rechazada|, FecHorRes=NOW(), Respuesta=|"+this.Razon+"|","solret","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
      this.CarLis();
      this.closeAddExpenseModal2.nativeElement.click();
    }).catch((err)=>{console.log(err)});
    
    /*
    this.servicios.AccSobBD("UPDATE","No","","","Estatus = |Rechazada|, FecHorRes=NOW(), Respuesta=|"+this.Razon+"|","solret","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.CarLis();
        
        thiss.closeAddExpenseModal2.nativeElement.click();
      }
    );
    */
  }



  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
      }
    );
    */
  }

  CarLisCue(UsuReg){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","datcue","WHERE NRegUsu = "+UsuReg,"ORDER BY Banco","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisCue = Res.data;
    }).catch((err)=>{console.log(err)});
    
    /*
    this.servicios.AccSobBD("SELECT","Si","*","","","datcue","WHERE NRegUsu = "+UsuReg,"ORDER BY Banco","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.LisCue = dataRes.data;
      }
    );
    */
  }

  Volver(){
    this.location.back();
  }

}