import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolretComponent } from './solret.component';

describe('SolretComponent', () => {
  let component: SolretComponent;
  let fixture: ComponentFixture<SolretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
