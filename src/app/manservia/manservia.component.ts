import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { MatDialog } from '@angular/material/dialog';

declare var google: any;

@Component({
  selector: 'app-manservia',
  templateUrl: './manservia.component.html',
  styleUrls: ['./manservia.component.scss']
})
export class ManserviaComponent implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  DirSer: any;
  DirDis: any;
  IcoOri: any;
  IcoDes: any;
  IcoPri: any;
  IcoSeg: any;
  IcoTer: any;
  infwinOri: any;
  infwinDes: any;
  infwinPri: any;
  infwinSeg: any;
  infwinTer: any;

  OriDes: string = "";
  OriLatLon: string = "";
  DesDes: string = "";
  DesLatLon: string = "";
  PriDes: string = "";
  PriLatLon: string = "";
  SegDes: string = "";
  SegLatLon: string = "";
  TerDes: string = "";
  TerLatLon: string = "";

  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  DetDat: any;
  DetEnc: any;

  DGM: any = {
    "Nombre":"Viajes",
    "CamMod":[
      {"Tit":"Cliente","Cmp":"NRegCli","Tip":"SelectQry","Req":"Si","VD":"","Val":"","Blo":"No","Qry":{"Cmps":"NRegistro,Nombre,Apellido","Tab":"usuarios","Don":"WHERE Tipo = |Cliente|","Ord":"ORDER BY Nombre,Apellido","Res":[]}},
      {"Tit":"Conductor","Cmp":"NRegCho","Tip":"SelectQry","Req":"Si","VD":"","Val":"","Blo":"No","Qry":{"Cmps":"NRegistro,Nombre,Apellido","Tab":"usuarios","Don":"WHERE Tipo = |Conductor|","Ord":"ORDER BY Nombre,Apellido","Res":[]}},
      {"Tit":"Distancia","Cmp":"Distancia","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Tiempo","Cmp":"Tiempo","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Precio","Cmp":"Precio","Tip":"Numero","Req":"Si","VD":"","Val":""},
      {"Tit":"Ganancia","Cmp":"Ganancia","Tip":"Porcentaje","Req":"Si","VD":"","Val":""},
      {"Tit":"Pago","Cmp":"ForPag","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Solicitado","Cmp":"FecHorSol","Tip":"FechaHora","Req":"Si","VD":"","Val":""},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activa","Tex":"Activa"},{"Val":"Inactiva","Tex":"Inactiva"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCli) DesCli,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCli) FotCli,(SELECT Telefono FROM usuarios WHERE NRegistro = A.NRegCli) TelCli,(SELECT Email FROM usuarios WHERE NRegistro = A.NRegCli) EmaCli,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCho AND NRegistro > 0) DesCho,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCho AND NRegistro > 0) FotCho,(SELECT Telefono FROM usuarios WHERE NRegistro = A.NRegCho AND NRegistro > 0) TelCho,(SELECT Email FROM usuarios WHERE NRegistro = A.NRegCho AND NRegistro > 0) EmaCho, (SELECT COUNT(*) FROM viajesenc WHERE NRegVia = A.NRegistro) TieEnc",
    "Tabla":"viajes A",
    "Donde":"",
    "Ordena":"ORDER BY NRegistro DESC",
    "TabFor":{"Fil":[1,2,3,4],"Col":[1,2,3,4]},
    "Carpeta":"cat"
  };

  constructor(
    public router: Router,
    public servicios: Servicios,
    public dialog: MatDialog
  ) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    
    this.CarCmpQry();
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  Documentos(Dat){
    this.DetDat = Dat;
    //this.CarDoc(this.DetDat.NRegistro);
  }

  VerEnc(Dat){
    this.DetDat = Dat;
    this.CarEnc(this.DetDat.NRegistro);
  }

  CarEnc(NRegVia){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","viajesenc","WHERE NRegVia="+NRegVia,"","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DetEnc = Res.data;
    }).catch((err)=>{console.log(err)});
  }

  Detalles(Dat){
    this.DetDat = Dat;

    this.showMap(
      function(thiss){
        thiss.OriDes = thiss.DetDat.OriDes;
        thiss.OriLatLon = thiss.DetDat.Origen;
        thiss.DesDes = thiss.DetDat.DesDes;
        thiss.DesLatLon = thiss.DetDat.Destino;
        thiss.PriDes = thiss.DetDat.PriDes;
        thiss.PriLatLon = thiss.DetDat.Primera;
        thiss.SegDes = thiss.DetDat.SegDes;
        thiss.SegLatLon = thiss.DetDat.Segunda;
        thiss.TerDes = thiss.DetDat.TerDes;
        thiss.TerLatLon = thiss.DetDat.Tercera;
        thiss.MosRutMap(thiss.DetDat.Origen,thiss.DetDat.Destino,thiss.DetDat.Primera,thiss.DetDat.Segunda,thiss.DetDat.Tercera);
      }
    );
  }

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          
          //console.log(dataRes);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }


  showMap(callback){
    var location;
    
    if (this.servicios.UsuMat.UbiAct != ""){
      var ULLM = this.servicios.UsuMat.UbiAct.split(",");
      location = new google.maps.LatLng(parseFloat(ULLM[0]),parseFloat(ULLM[1]));
      console.log(parseFloat(ULLM[0])+"/"+parseFloat(ULLM[1]))
    }else{
      //location = new google.maps.LatLng(-17.7655264,-63.226613);
      location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      styles: [
        /*
        {
          featureType: 'poi',
          stylers: [{visibility: 'off'}]
        },
        {
          featureType: 'transit.station',
          stylers: [{visibility: 'off'}]
        }
        */
      ]
    }

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    //const trafficLayer = new google.maps.TrafficLayer();
    //trafficLayer.setMap(this.map);
    callback(this);
  }

  MosRutMap(Ori,Des,Pri,Seg,Ter){
    this.DirSer = new google.maps.DirectionsService();
    this.DirDis = new google.maps.DirectionsRenderer(
      {
        suppressMarkers: true,
        polylineOptions: {strokeColor:'#154c68',strokeOpacity:0.7}
      }
    );

    var Waypts = [];

    if (Pri != ""){Waypts.push({location: Pri,stopover: true});}
    if (Seg != ""){Waypts.push({location: Seg,stopover: true});}
    if (Ter != ""){Waypts.push({location: Ter,stopover: true});}

    var dirdis = this.DirDis;
    this.AgrMarRut(Ori,Des,Pri,Seg,Ter);
    this.DirDis.setMap(this.map);
    this.DirSer.route({
      origin: Ori,
      destination: Des,
      //waypoints: Waypts,
      //optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        setTimeout(() => {
          dirdis.setDirections(response);
        }, 350);
        
        //console.log(response);
      }else{
        //console.log('Directions request failed due to ' + status);
      }
    });
  }

  AgrMarRut(Ori,Des,Pri,Seg,Ter){
    var OriSt = String(Ori);
    var MatOri = OriSt.split(",");
    var posOri = new google.maps.LatLng(MatOri[0],MatOri[1]);
    
    var DesSt = String(Des);
    var MatDes = DesSt.split(",");
    var posDes = new google.maps.LatLng(MatDes[0],MatDes[1]);

    if (Pri != ""){
      var PriSt = String(Pri);
      var MatPri = PriSt.split(",");
      var posPri = new google.maps.LatLng(MatPri[0],MatPri[1]);
    }

    if (Seg != ""){
      var SegSt = String(Seg);
      var MatSeg = SegSt.split(",");
      var posSeg = new google.maps.LatLng(MatSeg[0],MatSeg[1]);
    }

    if (Ter != ""){
      var TerSt = String(Ter);
      var MatTer = TerSt.split(",");
      var posTer = new google.maps.LatLng(MatTer[0],MatTer[1]);
    }

    this.infwinOri = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0,0)});
    this.infwinDes = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0,0)});
    
    if (Pri != ""){this.infwinPri = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0,0)});}
    if (Seg != ""){this.infwinSeg = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0,0)});}
    if (Ter != ""){this.infwinTer = new google.maps.InfoWindow({pixelOffset: new google.maps.Size(0,0)});}

    this.infwinOri.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.OriDes+"</td></tr></table></div>");
    this.infwinDes.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#ed7d31; text-align:center; vertical-align:middle;' id='tdDV'></td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.DesDes+"</td></tr></table></div>");

    if (Pri != ""){this.infwinPri.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.PriDes+"</td></tr></table></div>");}
    if (Seg != ""){this.infwinSeg.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.SegDes+"</td></tr></table></div>");}
    if (Ter != ""){this.infwinTer.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.TerDes+"</td></tr></table></div>");}

    var icosta = {
      url: this.servicios.ApiUrl+"img/mapa/ori.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    var icodes = {
      url: this.servicios.ApiUrl+"img/mapa/des.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    var icopar = {
      url: this.servicios.ApiUrl+"img/mapa/par.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    this.IcoOri = new google.maps.Marker({
      position: posOri,
      map: this.map,
      icon: icosta,
      title: "Origen"
    });
    //this.infwinOri.open(this.map,this.IcoOri);

    this.IcoDes = new google.maps.Marker({
      position: posDes,
      map: this.map,
      icon: icodes,
      title: "Destino"
    });
    //this.infwinDes.open(this.map,this.IcoDes);

    if (Pri != ""){
      this.IcoPri = new google.maps.Marker({
        position: posPri,
        map: this.map,
        icon: icopar,
        title: "Primera parada"
      });
      //this.infwinPri.open(this.map,this.IcoPri);
    }
    if (Seg != ""){
      this.IcoSeg = new google.maps.Marker({
        position: posSeg,
        map: this.map,
        icon: icopar,
        title: "Segunda parada"
      });
      //this.infwinSeg.open(this.map,this.IcoSeg);
    }
    if (Ter != ""){
      this.IcoTer = new google.maps.Marker({
        position: posTer,
        map: this.map,
        icon: icopar,
        title: "Tercera parada"
      });
      //this.infwinTer.open(this.map,this.IcoTer);
    }
  }
}