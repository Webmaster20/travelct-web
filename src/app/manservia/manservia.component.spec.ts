import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManserviaComponent } from './manservia.component';

describe('ManserviaComponent', () => {
  let component: ManserviaComponent;
  let fixture: ComponentFixture<ManserviaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManserviaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManserviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
