import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  REVNOT: any;
  HidBagNot: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit(): void {
    /*
    this.servicios.DatNot.Soporte.push({"a":0});
    this.HidBagNot = false;
    */

    if (!this.servicios.UsuMat.Nombre){
      this.router.navigate([""]);
    }else{
      if (!this.REVNOT){this.REVNOT = setInterval(() => this.RevNot(),10000);}
    }
  }

  RevNot(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","noti","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Estatus=|Nueva|","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (Res.data.length > 0){

        this.HidBagNot = false;

        this.servicios.DatNot.Total = Res.data;

        for (let n = 0; n < Res.data.length; n++) {
          switch (Res.data[n].Accion){
            case "Solicitudes":
              this.servicios.DatNot.Solicitudes.push(Res.data[n]);
            break;
            case "Servicios":
              this.servicios.DatNot.Servicios.push(Res.data[n]);
            break;
            case "Conductores":
              this.servicios.DatNot.Conductores.push(Res.data[n]);
            break;
            case "Chat":
              this.servicios.DatNot.Chat.push(Res.data[n]);
            break;
            case "Soporte":
              this.servicios.DatNot.Soporte.push(Res.data[n]);
            break;
            case "Panico":
              this.servicios.DatNot.Panico.push(Res.data[n]);
            break;
          }
        }

        this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Vista|","noti","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Estatus=|Nueva|","","",false).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(Res);
        }).catch((err)=>{console.log(err)});
      }
    }).catch((err)=>{console.log(err)});
  }

  VerDashGra(){
    this.router.navigate(["home/dasgra"]);
  }
  VerDashRes(){
    this.router.navigate(["home/dasres"]);
  }
  VerDashMap(){
    this.router.navigate(["home/dasmap"]);
  }



  VerSerVia(){
    this.router.navigate(["home/manservia"]);
  }
  VerSerTar(){
    this.router.navigate(["home/mansertar"]);
  }

  VerBill(){
    this.router.navigate(["home/billetera"]);
  }
  VerSolAbo(){
    this.router.navigate(["home/solabo"]);
  }
  VerSolRet(){
    this.router.navigate(["home/solret"]);
  }

  VerSerDoc(){
    this.router.navigate(["home/manserdoc"]);
  }
  VerDocReq(){
    this.router.navigate(["home/manserdocreq"]);
  }
  VerConNue(){
    this.router.navigate(["home/manusuconnue"]);
  }

  VerSerPub(){
    this.router.navigate(["home/publicidad"]);
  }

  VerSerMen(){
    this.router.navigate(["home/mansermen"]);
  }
  VerSerNot(){
    this.router.navigate(["home/noti"]);
  }

  VerDatEst(){
    this.router.navigate(["home/mandatest"]);
  }
  VerDatEnc(){
    this.router.navigate(["home/mandatenc"]);
  }
  VerDatPar(){
    this.router.navigate(["home/mandatpar"]);
  }

  VerSopPre(){
    this.router.navigate(["home/mansoppre"]);
  }
  VerSopPro(){
    this.router.navigate(["home/mansoppro"]);
  }

  VerUsuAdm(){
    this.router.navigate(["home/manusuadm"]);
  }
  VerUsuSop(){
    this.router.navigate(["home/manususop"]);
  }
  VerUsuSup(){
    this.router.navigate(["home/manususup"]);
  }
  VerUsuCli(){
    this.router.navigate(["home/manusucli"]);
  }
  VerUsuCon(){
    this.router.navigate(["home/manusucon"]);
  }

  VerOpcPre(){
    this.router.navigate(["home/perfil"]);
  }
  VerOpcAyu(){
    this.router.navigate(["home/ayuda"]);
  }
  VarOpcSal(){
    window.localStorage.setItem("token","");
    this.router.navigate([""]);
  }

  VerNot(){
    this.router.navigate(["home/noti"]);
    while(this.servicios.DatNot.Total.length > 0){this.servicios.DatNot.Total.pop();};
    this.HidBagNot = true;
  }

  TogNav() {
    if (document.getElementById("mySidenav").style.width == "0px"){
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("main").style.marginLeft = "250px";
      document.getElementById("topbar").style.marginLeft = "250px";
    }else{
      document.getElementById("mySidenav").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
      document.getElementById("topbar").style.marginLeft = "0px";
    }
  }
}
