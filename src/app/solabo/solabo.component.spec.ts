import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolaboComponent } from './solabo.component';

describe('SolaboComponent', () => {
  let component: SolaboComponent;
  let fixture: ComponentFixture<SolaboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolaboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
