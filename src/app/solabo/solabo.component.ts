import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-solabo',
  templateUrl: './solabo.component.html',
  styleUrls: ['./solabo.component.scss']
})
export class SolaboComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";

  DGM: any = {
    "Nombre":"Solicitud de abono",
    "CamMod":[
      {"Tit":"Usuario","Cmp":"DesUsu","Tip":"Texto","Req":"Si","VD":"","Val":"","SubQry":"(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Tipo","Cmp":"TipUsu","Tip":"Texto","Req":"Si","VD":"","Val":"","SubQry":"(SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Saldo","Cmp":"SalUsu","Tip":"Numero","Req":"Si","VD":"","Val":"","SubQry":"(SELECT Saldo FROM usuarios WHERE NRegistro = A.NRegUsu)"},
      {"Tit":"Descripción","Cmp":"Descripcion","Tip":"Texto","Req":"No","VD":"","Val":""},
      {"Tit":"Monto","Cmp":"Monto","Tip":"Numero","Req":"No","VD":"","Val":""},
      {"Tit":"Solicitado","Cmp":"FecHorReg","Tip":"FechaHora","Req":"No","VD":"","Val":""}
    ],
    "OpcFil":"Si",
    "CamQry":"*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu) DesUsu, (SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu) TipUsu, (SELECT Saldo FROM usuarios WHERE NRegistro = A.NRegUsu) SalUsu",
    "Tabla":"solabo A",
    "Donde":"WHERE Estatus = |Nueva|",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2,3,4],"Col":[1,2]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios, public location: Location) {}

  ngOnInit() {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  Aceptar(Dat){
    this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Transaccion,Descripcion,Monto,FecHor","|"+Dat.NRegUsu+"|,|Abono|,|"+Dat.Descripcion+"|,"+Dat.Monto+",NOW()","","billetera","","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.AccSobBDAA("UPDATE","No","","","Saldo = Saldo ¬ "+Dat.Monto,"usuarios","WHERE NRegistro = "+Dat.NRegUsu,"","",false).then((dataRes)=>{
        let ResB: any = dataRes;
        //console.log(ResB);
        this.servicios.AccSobBDAA("UPDATE","No","","","Estatus = |Aceptada|, FecHorRes=NOW()","solabo","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
          let ResC: any = dataRes;
          //console.log(ResC);
          this.DatRes = ResC.data;
          this.CarLis();
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("INSERT","No","NRegUsu,Transaccion,Descripcion,Monto,FecHor","|"+Dat.NRegUsu+"|,|Abono|,|"+Dat.Descripcion+"|,"+Dat.Monto+",NOW()","","billetera","","","","Ins",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.AccSobBD("UPDATE","No","","","Saldo = Saldo ¬ "+Dat.Monto,"usuarios","WHERE NRegistro = "+Dat.NRegUsu,"","","Upd",thiss,
          function(dataRes,thiss){
            //console.log(dataRes);
            thiss.servicios.AccSobBD("UPDATE","No","","","Estatus = |Aceptada|, FecHorRes=NOW()","solabo","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",thiss,
              function(dataRes,thiss){
                //console.log(dataRes);
                thiss.DatRes = dataRes.data;
                thiss.CarLis();
              }
            );
          }
        );
      }
    );
    */
  }

  Rechazar(Dat){
    this.servicios.AccSobBDAA("UPDATE","No","","","Estatus = |Rechazada|, FecHorRes=NOW()","solabo","WHERE NRegistro = "+Dat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
      this.CarLis();
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("UPDATE","No","","","Estatus = |Rechazada|, FecHorRes=NOW()","solabo","WHERE NRegistro = "+Dat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
        thiss.CarLis();
      }
    );
    */
  }



  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatRes = Res.data;
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.DatRes = dataRes.data;
      }
    );
    */
  }

  Volver(){
    this.location.back();
  }

}