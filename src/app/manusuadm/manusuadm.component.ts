import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-manusuadm',
  templateUrl: './manusuadm.component.html',
  styleUrls: ['./manusuadm.component.scss']
})
export class ManusuadmComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  DGM: any = {
    "Nombre":"Administradores",
    "CamMod":[
      {"Tit":"Foto Perfil","Cmp":"FotPer","Tip":"Imagen","Req":"Si","VD":"fpdu.png","Val":"","Est":"","Car":"usu"},
      {"Tit":"Nombre","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Apellido","Cmp":"Apellido","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Teléfono","Cmp":"Telefono","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Email","Cmp":"Email","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Password","Cmp":"Password","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Tipo","Cmp":"Tipo","Tip":"Texto","Req":"Si","VD":"Admin","Val":"","Blo":"Si"},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"","Val":"","Opcs":[{"Val":"Activo","Tex":"Activo"},{"Val":"Bloqueado","Tex":"Bloqueado"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"usuarios",
    "Donde":"WHERE Tipo = |Admin|",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2,3,4],"Col":[1,2]},
    "Carpeta":"usu"
  };

  constructor(public router: Router, public servicios: Servicios) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }
}