import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManusuadmComponent } from './manusuadm.component';

describe('ManusuadmComponent', () => {
  let component: ManusuadmComponent;
  let fixture: ComponentFixture<ManusuadmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManusuadmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManusuadmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
