import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';

declare var google: any;

@Component({
  selector: 'app-dashmap',
  templateUrl: './dashmap.component.html',
  styleUrls: ['./dashmap.component.scss']
})
export class DashmapComponent implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  map: any;

  ConDis: any = [];
  LisConDis: any;
  CarIni: boolean = false;
  TimCon: any;

  Lat: string = "";
  Lon: string = "";
  Ori: string = "";

  constructor(
    public servicios: Servicios,
  ){}

  ngOnInit(): void {
  }

  async ngAfterViewInit(){
    await this.showMap(
      function(thiss){
        thiss.CarConDis()
        if (!thiss.TimCon) {thiss.TimCon = setInterval(() => thiss.CarConDis(),5000);}
      }
    );
  }

  Mapa(){
    this.showMap(
      function(thiss){
        
      }
    );
  }

  showMap(callback){
    var location;
    location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);

    const options = {
      center: location,
      zoom: 7,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: true,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      styles: [
        /*
        {featureType: 'poi',stylers: [{visibility: 'off'}]},
        {featureType: 'transit.station',stylers: [{visibility: 'off'}]}
        */
      ]
    }

    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    this.map = map;

    /*
    const CliMar = new google.maps.Marker({
      id: "IDMCSCli",
      position: location,
      map: map
    });
    CliMar.setVisible(true);

    const geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow();
    */

    callback(this);
  }

  CarConDis(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE UbiAct!=|| AND Tipo=|Conductor|","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisConDis = Res.data;
      this.addConDis();
    }).catch((err)=>{console.log(err)});
  }

  addConDis(){
    var limits = new google.maps.LatLngBounds();
    for (var n=0; n<this.LisConDis.length; n++){
      if (this.LisConDis[n].UbiAct != ""){
        var MLL = this.LisConDis[n].UbiAct.split(",");
        var position = new google.maps.LatLng(MLL[0],MLL[1]);
        this.Ori = MLL[2];
        var icon;
        //console.log(this.LisConDis[n].UbiAct)
        //console.log(this.ConDis[n]);

        limits.extend(new google.maps.LatLng(MLL[0],MLL[1]));

        if (this.LisConDis[n].EstRep == "Desconectado"){
          icon = {url: "assets/img/logo.png",scaledSize: new google.maps.Size(36, 36),rotation:0,ID:"VD"+this.LisConDis[n].NRegistro};
        }else{
          icon = {url: "assets/img/logo.png",scaledSize: new google.maps.Size(36, 36),rotation:0,ID:"VD"+this.LisConDis[n].NRegistro};
        }


        if (this.ConDis[n] == undefined){
          this.ConDis[n] = new google.maps.Marker({
            id: "VD"+this.LisConDis[n].NRegistro,
            title: this.LisConDis[n].NRegistro,
            position: position,
            map: this.map,
            optimized: false,
            icon: icon
          });


          var conWor = "";
          if (this.LisConDis[n].EstCon == "Conectado"){
            conWor = "<div style='margin:5px; padding:5px; border-radius:5px; border-left:solid #fff; border-left-width:5px; border-left-color:#2dd36f;'>";
          }else{
            conWor = "<div style='margin:5px; padding:5px; border-radius:5px; border-left:solid #fff; border-left-width:5px; border-left-color:#eb445a;'>";
          }
          conWor = conWor + "<strong style='opacity:0.7;'>Nombre: </strong>" + this.LisConDis[n].Nombre+" "+this.LisConDis[n].Apellido + "<br>";
          conWor = conWor + "<strong style='opacity:0.7;'>Teléfono: </strong>" + this.LisConDis[n].Telefono + "<br>";
          conWor = conWor + "<strong style='opacity:0.7;'>Estatus: </strong>" + this.LisConDis[n].EstCon + "<br>";
          conWor = conWor + "</div>";

          const infowindow = new google.maps.InfoWindow({
            content: conWor
          });

          var MarExp = this.ConDis[n];

          (function(MarExp){
            google.maps.event.addListener(MarExp,'click', function(){
              //console.log(MarExp.id);
              infowindow.open(this.map,MarExp);
            });
          })(MarExp);



          var overlay = new google.maps.OverlayView()
          overlay.draw = function() {
            this.getPanes().markerLayer.id = 'markerLayer'
            //console.log(this.getPanes().markerLayer.children);
            for (var i=0; i<this.getPanes().markerLayer.children.length; i++){
              if (this.getPanes().markerLayer.children[i].g != undefined){
                //console.log(this.getPanes().markerLayer.children[i].g.ID+"***");
                this.getPanes().markerLayer.children[i].id = this.getPanes().markerLayer.children[i].g.ID;
              }
            }
          }
          overlay.setMap(this.map);

          //this.EstMarCho(this.ConDis[n].id,this.LisConDis[n].EstRep);
        }else{
          if (this.LisConDis[n].EstRep == "Desconectado"){
            icon = {url: "assets/img/logo.png",scaledSize: new google.maps.Size(36, 36),rotation:0,ID:"VD"+this.LisConDis[n].NRegistro};
          }else{
            icon = {url: "assets/img/logo.png",scaledSize: new google.maps.Size(36, 36),rotation:0,ID:"VD"+this.LisConDis[n].NRegistro};
          }
          this.ConDis[n].setIcon(icon);
          this.ConDis[n].setPosition(position);
          //this.EstMarCho(this.ConDis[n].id,this.LisConDis[n].EstRep);
          //console.log(this.ConDis[n].id+"---");
          //this.OriMarCho(this.ConDis[n].id,this.Ori);
        }
      }
    }
    if (this.LisConDis.length == 0){
      limits.extend(new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon));
      //limits.extend(new google.maps.LatLng(19.422310,-99.143089));
      //limits.extend(new google.maps.LatLng(19.442310,-99.143089));
      //limits.extend(new google.maps.LatLng(19.442310,-99.123089));
    }

    if (!this.CarIni){
      this.CarIni = true;
      setTimeout(() => {this.map.fitBounds(limits);}, 350);
    }
  }

}
