import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AyudaComponent } from './ayuda/ayuda.component';
import { BilleteraComponent } from './billetera/billetera.component';
import { DashgraComponent } from './dashgra/dashgra.component';
import { DashmapComponent } from './dashmap/dashmap.component';
import { DashresComponent } from './dashres/dashres.component';
import { FdaComponent } from './fda/fda.component';
import { HomeComponent } from './home/home.component';
import { IniComponent } from './ini/ini.component';
import { LoginComponent } from './login/login.component';
import { MandatestComponent } from './mandatest/mandatest.component';
import { MandatencComponent } from './mandatenc/mandatenc.component';
import { MandatparComponent } from './mandatpar/mandatpar.component';
import { MansermenComponent } from './mansermen/mansermen.component';
import { NotiComponent } from './noti/noti.component';
import { MansoppreComponent } from './mansoppre/mansoppre.component';
import { MansopproComponent } from './mansoppro/mansoppro.component';
import { ManusuadmComponent } from './manusuadm/manusuadm.component';
import { ManusucliComponent } from './manusucli/manusucli.component';
import { ManusuconComponent } from './manusucon/manusucon.component';
import { ManusuconnueComponent } from './manusuconnue/manusuconnue.component';
import { ManususopComponent } from './manususop/manususop.component';
import { ManususupComponent } from './manususup/manususup.component';
import { PerfilComponent } from './perfil/perfil.component';
import { SolaboComponent } from './solabo/solabo.component';
import { SolretComponent } from './solret/solret.component';
import { ManserviaComponent } from './manservia/manservia.component';
import { MansertarComponent } from './mansertar/mansertar.component';
import { ManserzonComponent } from './manserzon/manserzon.component';
import { ManserdocComponent } from './manserdoc/manserdoc.component';
import { ManserdocreqComponent } from './manserdocreq/manserdocreq.component';
import { PublicidadComponent } from './publicidad/publicidad.component';

const routes: Routes = [
  {path: "", component: IniComponent},
  {path: "login", component: LoginComponent},
  {path: "home", component: HomeComponent,
    children: [
      {path: '', component: DashmapComponent},
      {path: 'dasgra', component: DashgraComponent},
      {path: 'dasres', component: DashresComponent},
      {path: 'dasmap', component: DashmapComponent},
      {path: 'perfil', component: PerfilComponent},
      {path: 'ayuda', component: AyudaComponent},
      {path: 'fda', component: FdaComponent},
      {path: 'manusuadm', component: ManusuadmComponent},
      {path: 'manususop', component: ManususopComponent},
      {path: 'manususup', component: ManususupComponent},
      {path: 'manusucli', component: ManusucliComponent},
      {path: 'manusucon', component: ManusuconComponent},
      {path: 'manserdoc', component: ManserdocComponent},
      {path: 'manserdocreq', component: ManserdocreqComponent},
      {path: 'manusuconnue', component: ManusuconnueComponent},
      {path: 'mandatest', component: MandatestComponent},
      {path: 'mandatenc', component: MandatencComponent},
      {path: 'mandatpar', component: MandatparComponent},
      {path: 'mansoppre', component: MansoppreComponent},
      {path: 'mansoppro', component: MansopproComponent},

      {path: 'manservia', component: ManserviaComponent},
      {path: 'mansertar', component: MansertarComponent},
      {path: 'manserzon', component: ManserzonComponent},

      {path: 'publicidad', component: PublicidadComponent},
      {path: 'mansermen', component: MansermenComponent},
      {path: 'noti', component: NotiComponent},
      {path: 'billetera', component: BilleteraComponent},
      {path: 'solabo', component: SolaboComponent},
      {path: 'solret', component: SolretComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
