import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatparComponent } from './mandatpar.component';

describe('MandatparComponent', () => {
  let component: MandatparComponent;
  let fixture: ComponentFixture<MandatparComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatparComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatparComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
