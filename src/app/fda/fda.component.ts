import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Servicios } from '../../servicios/servicios';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-fda',
  templateUrl: './fda.component.html',
  styleUrls: ['./fda.component.scss']
})
export class FdaComponent implements OnInit {

  Cargando: string = "";
  fileToUpload: File = null;
  uploader: FileUploader = new FileUploader({ url: this.servicios.ApiUrl+"img/"+this.servicios.DGF.DGM.Carpeta+"/upload.php", removeAfterUpload: false, autoUpload: true });
  IDInpFil: number;

  constructor(public router: Router, public servicios: Servicios, public location: Location, private cd: ChangeDetectorRef) {
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log(response);
      var responsePath = JSON.parse(response);
      this.servicios.DGF.DGM.CamMod[this.IDInpFil].Val = responsePath.name;
      this.servicios.DGF.DGM.CamMod[this.IDInpFil].Est = "";
      this.Cargando = "";
    };
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
    this.uploader.onAfterAddingFile = (item => {
      item.withCredentials = false;
   });
  }

  Ejecuta(Acc){
    let Acci = "";
    let CmpS = "";
    let ValS = "";
    let CyVS = "";
    let Dond = "";

    if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.servicios.DGF.DGM.CamMod.length; n2++){
        if (this.servicios.DGF.DGM.CamMod[n2].Val != ""){
          if (Dond == ""){
            if (this.servicios.DGF.DGM.CamMod[n2].Cmp.substr(0,3) == "Fec"){
              Dond = this.servicios.DGF.DGM.CamMod[n2].Cmp+" LIKE |"+this.servicios.DGF.DGM.CamMod[n2].Val.substr(0,10)+"·|";
            }else if (this.servicios.DGF.DGM.CamMod[n2].SubQry){
              Dond = this.servicios.DGF.DGM.CamMod[n2].SubQry+" LIKE |·"+this.servicios.DGF.DGM.CamMod[n2].Val+"·|";
            }else{
              Dond = this.servicios.DGF.DGM.CamMod[n2].Cmp+" LIKE |·"+this.servicios.DGF.DGM.CamMod[n2].Val+"·|";
            }
          }else{
            if (this.servicios.DGF.DGM.CamMod[n2].Cmp.substr(0,3) == "Fec"){
              Dond = Dond+" AND "+this.servicios.DGF.DGM.CamMod[n2].Cmp+" LIKE |"+this.servicios.DGF.DGM.CamMod[n2].Val.substr(0,10)+"·|";
            }else if (this.servicios.DGF.DGM.CamMod[n2].SubQry){
              Dond = Dond+" AND "+this.servicios.DGF.DGM.CamMod[n2].SubQry+" LIKE |·"+this.servicios.DGF.DGM.CamMod[n2].Val+"·|";
            }else{
              Dond = Dond+" AND "+this.servicios.DGF.DGM.CamMod[n2].Cmp+" LIKE |·"+this.servicios.DGF.DGM.CamMod[n2].Val+"·|";
            }
          }
        }
      }

      window.localStorage.setItem("buscar"+this.servicios.DGF.DGM.Nombre,Dond);
      this.location.back();
      //console.log(Dond);
      return;
    }

    for (var n = 0; n < this.servicios.DGF.DGM.CamMod.length; n++){
      if (!this.servicios.ValCam("IB"+n,this.servicios.DGF.DGM.CamMod[n].Tit,this.servicios.DGF.DGM.CamMod[n].Tip,this.servicios.DGF.DGM.CamMod[n].Val,this.servicios.DGF.DGM.CamMod[n].Req)){return;}

      if (n == 0){
        CmpS = this.servicios.DGF.DGM.CamMod[n].Cmp;
        ValS = "|"+this.servicios.DGF.DGM.CamMod[n].Val+"|";
        CyVS = this.servicios.DGF.DGM.CamMod[n].Cmp+"="+"|"+this.servicios.DGF.DGM.CamMod[n].Val+"|";
      }else{
        CmpS = CmpS+","+this.servicios.DGF.DGM.CamMod[n].Cmp;
        ValS = ValS+",|"+this.servicios.DGF.DGM.CamMod[n].Val+"|";
        CyVS = CyVS+","+this.servicios.DGF.DGM.CamMod[n].Cmp+"="+"|"+this.servicios.DGF.DGM.CamMod[n].Val+"|";
      }
    }

    ValS = ValS.replace("+","¬");
    CyVS = CyVS.replace("+","¬");

    switch(Acc){
      case "Agregar":
        Acci = "INSERT";
      break;
      case "Modificar":
        Acci = "UPDATE";
        Dond = "WHERE NRegistro = "+this.servicios.DGF.Dat.NRegistro;
      break;
      case "Eliminar":
        Acci = "DELETE";
        Dond = "WHERE NRegistro = "+this.servicios.DGF.Dat.NRegistro;
      break;
    }
    
    /*
    console.log(CmpS);
    console.log(ValS);
    console.log(CyVS);
    console.log(Dond);
    console.log(Acci);
    */

    this.servicios.AccSobBDAA(Acci,"No",CmpS,ValS,CyVS,this.servicios.DGF.DGM.Tabla,Dond,"","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.location.back();
    }).catch((err)=>{console.log(err)});

    /*
    this.servicios.AccSobBD(Acci,"No",CmpS,ValS,CyVS,this.servicios.DGF.DGM.Tabla,Dond,"","","CarVar",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.location.back();
      }
    );
    */
  }

  CarFot(IFID){
    let IDI = "IB"+IFID;
    console.log(IDI);
    //this.InpFot.nativeElement.click();
    let OBJID = document.getElementById(IDI);
    OBJID.click();
  }
  ActVal(IndFil){
    this.IDInpFil = IndFil;
    this.servicios.DGF.DGM.CamMod[this.IDInpFil].Est = "Cargando";
    this.Cargando = "Si";
  }

  Volver(){
    this.location.back();
  }
}