import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FdaComponent } from './fda.component';

describe('FdaComponent', () => {
  let component: FdaComponent;
  let fixture: ComponentFixture<FdaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FdaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FdaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
