import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManususopComponent } from './manususop.component';

describe('ManususopComponent', () => {
  let component: ManususopComponent;
  let fixture: ComponentFixture<ManususopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManususopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManususopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
