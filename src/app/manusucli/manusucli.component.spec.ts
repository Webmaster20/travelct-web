import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManusucliComponent } from './manusucli.component';

describe('ManusucliComponent', () => {
  let component: ManusucliComponent;
  let fixture: ComponentFixture<ManusucliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManusucliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManusucliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
