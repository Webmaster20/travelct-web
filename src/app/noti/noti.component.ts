import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-noti',
  templateUrl: './noti.component.html',
  styleUrls: ['./noti.component.scss']
})
export class NotiComponent implements OnInit {

  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  DGM: any = {
    "Nombre":"Notificaciones",
    "CamMod":[
      {"Tit":"Titulo","Cmp":"Titulo","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Contenido","Cmp":"Contenido","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Fecha Hora","Cmp":"FecHor","Tip":"FechaHora","Req":"No","VD":"","Val":""},
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"noti",
    "Donde":"WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,
    "Ordena":"ORDER BY NRegistro DESC",
    "TabFor":{"Fil":[1,2,3],"Col":[1,2]},
    "Carpeta":"ser"
  };

  constructor(public router: Router, public servicios: Servicios) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarCmpQry();
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  VerDet(Dat){
    console.log(Dat);
    switch(Dat.Accion){
      case "Conductores":
        if (Dat.Titulo == "Nuevo Conductor"){
          this.router.navigate(["home/manusuconnue"]);
        }else if (Dat.Titulo == "Nuevo Documento"){
          this.router.navigate(["home/manserdoc"]);
        }
        
      break;

      case "Viajes":
        if (Dat.Titulo == "Vieje Iniciado"){
          this.router.navigate(["home/manservia"]);
        }
        
      break;

      case "Billetera":
        if (Dat.Titulo == "Nuevo Abono"){
          this.router.navigate(["home/solabo"]);
        }
        if (Dat.Titulo == "Nuevo Retiro"){
          this.router.navigate(["home/solret"]);
        }
        
      break;
    }
  }

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          
          //console.log(dataRes);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }
}