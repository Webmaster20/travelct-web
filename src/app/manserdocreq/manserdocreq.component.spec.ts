import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManserdocreqComponent } from './manserdocreq.component';

describe('ManserdocreqComponent', () => {
  let component: ManserdocreqComponent;
  let fixture: ComponentFixture<ManserdocreqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManserdocreqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManserdocreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
