import { Component } from '@angular/core';

import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = this.servicios.App.Nombre;

  constructor(public servicios: Servicios) {}
}
