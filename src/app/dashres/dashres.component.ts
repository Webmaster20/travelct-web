import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-dashres',
  templateUrl: './dashres.component.html',
  styleUrls: ['./dashres.component.scss']
})
export class DashresComponent implements OnInit {

  FAD = new Date();
  FecAnt: string = "";
  FecAct: string = new Date().toISOString();
  FecDes: string = this.FecAct.substr(0,10);
  FecHas: string = this.FecAct.substr(0,10);

  TotNueCli: number = 0;
  TotNueCon: number = 0;
  TotVia: number = 0;
  TotSer: number = 0;
  TotGan: number = 0;

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit(): void {
    this.FAD = new Date();
    this.FAD.setMonth(this.FAD.getMonth() - 1);
    this.FecAnt = this.FAD.toISOString();
    this.FecDes = this.FecAnt.substr(0,10);
    
    this.ActRes();
  }

  ActRes(){
    this.CalTotNueCli();
    this.CalTotNueCon();
    this.CalTotVia();
    //this.CalTotSer();
    this.CalTotGan();
  }

  VerRes1(){
    this.router.navigate(["home/manusucli"]);
  }
  VerRes2(){
    this.router.navigate(["home/manusucon"]);
  }
  VerRes3(){
    this.router.navigate(["home/manservia"]);
  }
  VerRes4(){
    this.router.navigate(["home/billetera"]);
  }
  VerRes5(){
    
  }

  CalTotNueCli(){
    this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) Tot","","","usuarios","WHERE Tipo=|Cliente| AND FecHorReg BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotNueCli = Res.data[0].Tot;
    }).catch((err)=>{console.log(err)});
  }
  CalTotNueCon(){
    this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) Tot","","","usuarios","WHERE Tipo=|Conductor| AND FecHorReg BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotNueCon = Res.data[0].Tot;
    }).catch((err)=>{console.log(err)});
  }
  CalTotVia(){
    this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) Tot","","","viajes","WHERE Estatus=|Terminado| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotVia = Res.data[0].Tot;
    }).catch((err)=>{console.log(err)});
  }
  CalTotSer(){
    this.servicios.AccSobBDAA("SELECT","Si","COUNT(*) Tot","","","proser","WHERE FecReg BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotSer = Res.data[0].Tot;
    }).catch((err)=>{console.log(err)});
  }
  CalTotGan(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Ganancia) Tot","","","viajes","WHERE Estatus=|Terminado| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotGan = Res.data[0].Tot;
    }).catch((err)=>{console.log(err)});
  }

  CarLisExp(){}
}
