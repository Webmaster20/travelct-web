import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashresComponent } from './dashres.component';

describe('DashresComponent', () => {
  let component: DashresComponent;
  let fixture: ComponentFixture<DashresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
