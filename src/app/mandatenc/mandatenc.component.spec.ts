import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatencComponent } from './mandatenc.component';

describe('MandatencComponent', () => {
  let component: MandatencComponent;
  let fixture: ComponentFixture<MandatencComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatencComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatencComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
