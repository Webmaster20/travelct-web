import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MansopproComponent } from './mansoppro.component';

describe('MansopproComponent', () => {
  let component: MansopproComponent;
  let fixture: ComponentFixture<MansopproComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MansopproComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MansopproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
