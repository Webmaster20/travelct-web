import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';

import { Servicios } from '../servicios/servicios';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { FileUploadModule } from 'ng2-file-upload';

import { IniComponent } from './ini/ini.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { DashgraComponent } from './dashgra/dashgra.component';
import { DashresComponent } from './dashres/dashres.component';
import { DashmapComponent } from './dashmap/dashmap.component';
import { PerfilComponent } from './perfil/perfil.component';
import { ManusuadmComponent } from './manusuadm/manusuadm.component';
import { FdaComponent } from './fda/fda.component';
import { ManususopComponent } from './manususop/manususop.component';
import { ManususupComponent } from './manususup/manususup.component';
import { ManusucliComponent } from './manusucli/manusucli.component';
import { MansoppreComponent } from './mansoppre/mansoppre.component';
import { MansopproComponent } from './mansoppro/mansoppro.component';
import { MandatparComponent } from './mandatpar/mandatpar.component';
import { MansermenComponent } from './mansermen/mansermen.component';
import { AyudaComponent } from './ayuda/ayuda.component';
import { BilleteraComponent } from './billetera/billetera.component';
import { SolaboComponent } from './solabo/solabo.component';
import { SolretComponent } from './solret/solret.component';
import { NotiComponent } from './noti/noti.component';
import { ManserviaComponent } from './manservia/manservia.component';
import { MansertarComponent } from './mansertar/mansertar.component';
import { ManserzonComponent } from './manserzon/manserzon.component';
import { ManusuconComponent } from './manusucon/manusucon.component';
import { ManusuconnueComponent } from './manusuconnue/manusuconnue.component';
import { ManserdocComponent } from './manserdoc/manserdoc.component';
import { ManserdocreqComponent } from './manserdocreq/manserdocreq.component';
import { MandatencComponent } from './mandatenc/mandatenc.component';
import { MandatestComponent } from './mandatest/mandatest.component';
import { PublicidadComponent } from './publicidad/publicidad.component';

@NgModule({
  declarations: [
    AppComponent,
    IniComponent,
    LoginComponent,
    HomeComponent,
    DashgraComponent,
    DashresComponent,
    DashmapComponent,
    PerfilComponent,
    ManusuadmComponent,
    FdaComponent,
    ManususopComponent,
    ManususupComponent,
    ManusucliComponent,
    MansoppreComponent,
    MansopproComponent,
    MandatparComponent,
    MansermenComponent,
    AyudaComponent,
    BilleteraComponent,
    SolaboComponent,
    SolretComponent,
    NotiComponent,
    ManserviaComponent,
    MansertarComponent,
    ManserzonComponent,
    ManusuconComponent,
    ManusuconnueComponent,
    ManserdocComponent,
    ManserdocreqComponent,
    MandatencComponent,
    MandatestComponent,
    PublicidadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatGridListModule,
    MatCardModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatExpansionModule,
    MatIconModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    NgbModule,
    ChartsModule,
    FileUploadModule
  ],
  providers: [Servicios],
  bootstrap: [AppComponent]
})
export class AppModule { }
