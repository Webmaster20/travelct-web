import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManususupComponent } from './manususup.component';

describe('ManususupComponent', () => {
  let component: ManususupComponent;
  let fixture: ComponentFixture<ManususupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManususupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManususupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
