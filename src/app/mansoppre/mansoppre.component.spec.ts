import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MansoppreComponent } from './mansoppre.component';

describe('MansoppreComponent', () => {
  let component: MansoppreComponent;
  let fixture: ComponentFixture<MansoppreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MansoppreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MansoppreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
