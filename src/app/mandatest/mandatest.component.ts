import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

declare var google: any;

@Component({
  selector: 'app-mandatest',
  templateUrl: './mandatest.component.html',
  styleUrls: ['./mandatest.component.scss']
})
export class MandatestComponent implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;


  DatRes: any;
  DonBus: string = "";
  OJE: any = [];

  map: any;
  DetDat: any;


  DGM: any = {
    "Nombre":"Estaciones de Servicio",
    "CamMod":[
      {"Tit":"Nombre","Cmp":"Nombre","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Descripcion","Cmp":"Descripcion","Tip":"Texto","Req":"Si","VD":"","Val":""},
      {"Tit":"Direccion","Cmp":"Direccion","Tip":"Texto","Req":"No","VD":"","Val":"","Blo":"Si"},
      {"Tit":"Estado","Cmp":"Estatus","Tip":"Select","Req":"Si","VD":"Inactiva","Val":"","Blo":"Si","Opcs":[{"Val":"Activa","Tex":"Activa"},{"Val":"Inactiva","Tex":"Inactiva"}]}
    ],
    "OpcFil":"Si",
    "CamQry":"*",
    "Tabla":"estaciones",
    "Donde":"",
    "Ordena":"ORDER BY NRegistro",
    "TabFor":{"Fil":[1,2,3],"Col":[1,2]},
    "Carpeta":""
  };

  constructor(public router: Router, public servicios: Servicios) {

  }

  ngOnInit(): void {
    //window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    //console.log(window.localStorage.getItem("buscar"+this.DGM.Nombre));
    if(window.localStorage.getItem("buscar"+this.DGM.Nombre) != "" && window.localStorage.getItem("buscar"+this.DGM.Nombre) != null){
      if (this.DGM.Donde == ""){
        this.DonBus = "WHERE " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }else{
        this.DonBus = " AND " + window.localStorage.getItem("buscar"+this.DGM.Nombre);
      }
    }else{
      this.DonBus = "";
    }
    this.CarCmpQry();
    this.CarLis();
  }
  ngAfterViewInit(){
    
  }
  Refrescar(){
    window.localStorage.setItem("buscar"+this.DGM.Nombre,"");
    this.DonBus = "";
    this.DatRes = null;
    this.CarLis();
  }

  //////////////////////////////////////////////////////////////////////////////////////
  Delivery(Dat){
    this.DetDat = Dat;
    this.showMap(
      function(thiss){
        //thiss.MosGeoMap(thiss.DetDat);
        //console.log("Hola");
      }
    );
  }

  showMap(callback){
    if (this.DetDat.Ubicacion != ""){
      let MLL = this.DetDat.Ubicacion.split(",");
      var location = new google.maps.LatLng(MLL[0],MLL[1]);
    }else{
      var location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: true,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      styles: [
        /*
        {
          featureType: 'poi',
          stylers: [{visibility: 'off'}]
        },
        {
          featureType: 'transit.station',
          stylers: [{visibility: 'off'}]
        }
        */
      ]
    }

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);

    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    const geocoder = new google.maps.Geocoder;
    const CliMar = new google.maps.Marker({
      id: "IDMCSCli",
      position: location,
      map: map
    });
    //CliMar.setVisible(false);
    
    var input = window.document.getElementById('pac-input');
    var infowindow = new google.maps.InfoWindow();
    var autocomplete = new google.maps.places.Autocomplete(input);

    map.addListener('click', function(e) {
      CliMar.setPosition(e.latLng);
      CliMar.setVisible(true);
      map.panTo(CliMar.getPosition());
      map.setZoom(16);
      infowindow.setContent("...");
      geocoder.geocode({'location': e.latLng}, function(results, status) {
        if (status === 'OK'){
          if (results[0]) {
            infowindow.setContent(results[0].formatted_address);
            document.getElementById("BusDes").innerHTML = results[0].formatted_address;
            document.getElementById("BusLatLon").innerHTML = e.latLng.lat()+","+e.latLng.lng();
            infowindow.open(map, CliMar);
          }else{
            document.getElementById("BusDes").innerHTML = "Sin resultados";
          }
        }else{
          document.getElementById("BusDes").innerHTML = "Sin resultados";
        }
      });
    });

    autocomplete.bindTo('bounds', map);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    infowindow.setContent("...");

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      CliMar.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
      CliMar.setPosition(place.geometry.location);
      CliMar.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      document.getElementById("BusDes").innerHTML = address;
      document.getElementById("BusLatLon").innerHTML = place.geometry.location.lat()+","+place.geometry.location.lng();

      infowindow.setContent(address);
      infowindow.open(map, CliMar);
    })

    //const trafficLayer = new google.maps.TrafficLayer();
    //trafficLayer.setMap(this.map);
    callback(this);
  }

  GuaUbiDel(){
    if (document.getElementById("BusLatLon").innerHTML == ""){this.servicios.EnvMsgSim("Pos favor haga click en la Ubicación Delivery de "+this.DetDat.Nombre,"Advertencia"); return;}
    //console.log(document.getElementById("BusDes").innerHTML);
    //console.log(document.getElementById("BusLatLon").innerHTML);
    this.servicios.AccSobBDAA("UPDATE","No","","","Ubicacion=|"+document.getElementById("BusLatLon").innerHTML+"|,Direccion=|"+document.getElementById("BusDes").innerHTML+"|,Estatus=|Activa|","estaciones","WHERE NRegistro = "+this.DetDat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
      this.closeAddExpenseModal.nativeElement.click();
      this.CarLis();
    }).catch((err)=>{console.log(err)});
    /*
    this.servicios.AccSobBD("UPDATE","No","","","UbiDel=|"+document.getElementById("BusLatLon").innerHTML+"|,DesDel=|"+document.getElementById("BusDes").innerHTML+"|","usuarios","WHERE NRegistro = "+this.DetDat.NRegistro,"","","Upd",this,
      function(dataRes,thiss){
        //console.log(dataRes);
        thiss.servicios.EnvMsgSim("Los cambios fueron guardados","Informacion");
        thiss.closeAddExpenseModal.nativeElement.click();
        thiss.CarLis();
      }
    );
    */
  }
  //////////////////////////////////////////////////////////////////////////////////////

  EjeFun(Acc,Dat){
    if (Acc == "Agregar"){
      for (var n = 0; n < this.DGM.CamMod.length; n++){
        this.DGM.CamMod[n].Val = this.DGM.CamMod[n].VD;
      }

    }else if (Acc == "Buscar"){
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        this.DGM.CamMod[n2].Val = "";
      }

    }else{
      for(let Cam in Dat){
        for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
          if(Cam == this.DGM.CamMod[n2].Cmp){
            this.DGM.CamMod[n2].Val = Dat[Cam];
          }
        }
      }
    }

    this.servicios.DGF = {Acc:Acc,Dat:Dat,DGM:this.DGM};
    this.router.navigate(["home/fda"]);
  }

  CarCmpQry(){
    for (var n = 0; n < this.DGM.CamMod.length; n++){
      if (this.DGM.CamMod[n].Tip == "SelectQry"){
        let IDC:number = n;

        this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamMod[n].Qry.Cmps,"","",this.DGM.CamMod[n].Qry.Tab,this.DGM.CamMod[n].Qry.Don,this.DGM.CamMod[n].Qry.Ord,"",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          
          //console.log(dataRes);
          this.DGM.CamMod[IDC].Qry.Res = Res.data;
        }).catch((err)=>{console.log(err)});
      }
    }
  }

  CarLis(){
    this.servicios.AccSobBDAA("SELECT","Si",this.DGM.CamQry,"","",this.DGM.Tabla,this.DGM.Donde+this.DonBus,this.DGM.Ordena,"",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      
      this.DatRes = Res.data;
      this.CarLisExp();
    }).catch((err)=>{console.log(err)});
  }
  CarLisExp(){
    for (var n = 0; n < this.DatRes.length; n++){
      let BOJE = {};
      for (var n2 = 0; n2 < this.DGM.CamMod.length; n2++){
        BOJE[this.DGM.CamMod[n2].Tit] = this.servicios.ForCamMod(this.DatRes[n][this.DGM.CamMod[n2].Cmp],this.DGM.CamMod[n2]);
      }
      this.OJE.push(BOJE);
    }
  }
}