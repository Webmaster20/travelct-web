import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatestComponent } from './mandatest.component';

describe('MandatestComponent', () => {
  let component: MandatestComponent;
  let fixture: ComponentFixture<MandatestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
