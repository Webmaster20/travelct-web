import { Component, OnInit } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.scss']
})
export class AyudaComponent implements OnInit {

  LisTem: any;
  LisPre: any;

  Tema: string = "";
  TemSel: string = "";
  PreSel: string = "";
  ResSel: string = "";

  constructor(public servicios: Servicios) {
  }

  ngOnInit() {
    this.CarLisTem();
  }

  SelTem(Tem){
    console.log(Tem);
    this.TemSel = Tem.Tema;
    this.LisPre = [];
    this.PreSel = "";
    this.ResSel = "";
    this.CarLisPre();
  }

  SelPre(Pre){
    console.log(Pre);
    this.PreSel = Pre.Pregunta;
    this.ResSel = Pre.Respuesta;
  }

  CarLisTem(){
    this.servicios.AccSobBDAA("SELECT","Si","DISTINCT(Tema)","","","prefre","WHERE Usuario=|"+this.servicios.UsuMat.Tipo+"| AND Estatus=|Activa|","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisTem = Res.data;
    }).catch((err)=>{console.log(err)});
  }

  CarLisPre(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","prefre","WHERE Usuario = |"+this.servicios.UsuMat.Tipo+"| AND Tema = |"+this.TemSel+"| AND Estatus = |Activa|","ORDER BY Tema","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisPre = Res.data;
    }).catch((err)=>{console.log(err)});
  }
}