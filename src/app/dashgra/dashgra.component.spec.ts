import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashgraComponent } from './dashgra.component';

describe('DashgraComponent', () => {
  let component: DashgraComponent;
  let fixture: ComponentFixture<DashgraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashgraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashgraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
