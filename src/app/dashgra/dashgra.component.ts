import { Component, OnInit, ViewChild } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';

import { ChartsModule, BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-dashgra',
  templateUrl: './dashgra.component.html',
  styleUrls: ['./dashgra.component.scss']
})
export class DashgraComponent implements OnInit {

  @ViewChild('Gra1') Gra1 : BaseChartDirective;
  @ViewChild('Gra2') Gra2 : BaseChartDirective;
  @ViewChild('Gra3') Gra3 : BaseChartDirective;

  FAD = new Date();
  FecAnt: string = "";
  FecAct: string = new Date().toISOString();
  FecDes: string = this.FecAct.substr(0,10);
  FecHas: string = this.FecAct.substr(0,10);

  ////////////////////////////////////////////////////////
  DatGraSer: any;
  DatGraCat: any;
  DatGraHor: any;
  DatGraHorB: any;
  DatGraHorC: any;
  DatGraHorD: any;
  DatGraHorE: any;
  ////////////////////////////////////////////////////////

  donaChartLabels: string[] = ["Nuevo","Aceptado","Iniciado","Terminado","Cancelado"];
  donaChartData: number[] = [0,0,0,0,0];
  donaChartType: string = 'doughnut';
  donaChartOpt: any = {responsive:true, maintainAspectRatio:false, legend:{position:'bottom'}};
  donaChartCol: any[] = [{
    backgroundColor: ["#60d49b","#29a5dc","#9064f3","#32ad32","#f25213"],
    borderColor: 'rgba(255,255,255,0.5)'
  }];

  ///////////////////////////////////////////////////////

  barChartLabels: string[] = ["Cliente","Conductor"];
  barChartData: number[] = [0,0];
  //barChartData: Array<any> = [{data:0,label:"Cliente"},{data:0,label:"Vendedor"},{data:0,label:"Repartidor"}];
  barChartType: string = 'bar';
  barChartOpt: any = {responsive:true,scales:{yAxes:[{ticks:{beginAtZero:true}}]}, maintainAspectRatio:false, legend:{display:false}};
  barChartCol: any[] = [{
    backgroundColor: ["#aaf386","#86c7f3","#64a5f3","#9064f3","#646bf3"],
    borderColor: 'rgba(255,255,255,0.5)'
  }];

  ///////////////////////////////////////////////////////
  lineChartLabels: Array<any> = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"];

  lineChartData:Array<any> = [
    {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Nuevo"},
    {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Aceptado"},
    {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Iniciado"},
    {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Terminado"},
    {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Cancelado"}
  ];

  lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio:false
  };
  lineChartColors: Array<any> = [
    {
      backgroundColor: "rgba(96,212,155,0.2)",
      borderColor: "rgba(96,212,155,1)",
      pointBackgroundColor: "rgba(96,212,155,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(96,212,155,0.8)"
    },
    {
      backgroundColor: "rgba(100,165,243,0.2)",
      borderColor: "rgba(100,165,243,1)",
      pointBackgroundColor: "rgba(100,165,243,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(100,165,243,0.8)"
    },
    {
      backgroundColor: "rgba(144,100,243,0.2)",
      borderColor: "rgba(144,100,243,1)",
      pointBackgroundColor: "rgba(144,100,243,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(144,100,243,0.8)"
    },
    {
      backgroundColor: "rgba(170,243,134,0.2)",
      borderColor: "rgba(170,243,134,1)",
      pointBackgroundColor: "rgba(170,243,134,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(170,243,134,0.8)"
    },
    {
      backgroundColor: "rgba(242,82,19,0.2)",
      borderColor: "rgba(242,82,19,1)",
      pointBackgroundColor: "rgba(242,82,19,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(242,82,19,0.8)"
    }
  ];
  lineChartLegend: boolean = true;
  lineChartType: string = "line";


  constructor(
    public servicios: Servicios,
    public Chart: ChartsModule
  ) { }

  ngOnInit(): void {
    this.FAD = new Date();
    this.FAD.setMonth(this.FAD.getMonth() - 1);
    this.FecAnt = this.FAD.toISOString();
    this.FecDes = this.FecAnt.substr(0,10);
    
    this.ActGra();
  }

  ActGra(){
    this.CarDatGraEst();
    this.CarDatGraUsu();
    this.CarDatGraHor();
  }

  CarDatGraEst(){
    this.servicios.AccSobBDAA("SELECT","Si","Estatus,COUNT(Estatus)","","","viajes","WHERE FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY Estatus ORDER BY Estatus","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatGraSer = Res.data;
      this.donaChartLabels = ["Nuevo","Aceptado","Iniciado","Terminado","Cancelado"];
      this.donaChartData = [0,0,0,0,0];
      var Lab = this.donaChartLabels;
      var Dat = this.donaChartData;
      for (var i=0; i<this.DatGraSer.length; i++){
        for (var i2=0; i2<Lab.length; i2++){
          if(Lab[i2] == this.DatGraSer[i][0]){
            Dat[i2] = parseFloat(this.DatGraSer[i][1]);
            this.donaChartData[i2] = parseFloat(this.DatGraSer[i][1]);
          }else if(Lab[i2] == "Cancelado" && (this.DatGraSer[i][0] == "Cancelado Cliente")){
            Dat[i2] = Dat[i2] + parseFloat(this.DatGraSer[i][1]);
            this.donaChartData[i2] = parseFloat(this.DatGraSer[i][1]);
          }
        }
      }
    }).catch((err)=>{console.log(err)});
  }

  CarDatGraUsu(){
    this.servicios.AccSobBDAA("SELECT","Si","Tipo,COUNT(Tipo) Can","","","usuarios A","WHERE Tipo IN (|Cliente|,|Conductor|)","GROUP BY Tipo ORDER BY Tipo","",true).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.DatGraCat = Res.data;
        this.barChartLabels = ["Cliente","Conductor"];
        this.barChartData = [0,0];
        var Lab = this.barChartLabels;
        var Dat = this.barChartData;
        for (var i=0; i<this.DatGraCat.length; i++){
          for (var i2=0; i2<Lab.length; i2++){
            if(Lab[i2] == this.DatGraCat[i].Tipo){
              Dat[i2] = parseFloat(this.DatGraCat[i].Can);
              this.barChartData[i2] = parseFloat(this.DatGraCat[i].Can);
            }
          }
        }
    }).catch((err)=>{console.log(err)});
  }

  CarDatGraHor(){
    this.lineChartLabels = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23"];
    this.lineChartData = [
      {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Nuevo"},
      {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Aceptado"},
      {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Iniciado"},
      {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Terminado"},
      {data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: "Cancelado"}
    ];

    this.servicios.AccSobBDAA("SELECT","Si","HOUR(FecHorSol),COUNT(*),Estatus","","","viajes A","WHERE Estatus=|Nuevo| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY HOUR(FecHorSol) ORDER BY HOUR(FecHorSol)","",true).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatGraHor = Res.data;

      this.servicios.AccSobBDAA("SELECT","Si","HOUR(FecHorSol),COUNT(*),Estatus","","","viajes A","WHERE Estatus=|Aceptado| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY HOUR(FecHorSol) ORDER BY HOUR(FecHorSol)","",true).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.DatGraHorB = Res.data;
  
        this.servicios.AccSobBDAA("SELECT","Si","HOUR(FecHorSol),COUNT(*),Estatus","","","viajes A","WHERE Estatus=|Iniciado| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY HOUR(FecHorSol) ORDER BY HOUR(FecHorSol)","",true).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.DatGraHorC = Res.data;

          this.servicios.AccSobBDAA("SELECT","Si","HOUR(FecHorSol),COUNT(*),Estatus","","","viajes A","WHERE Estatus=|Terminado| AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY HOUR(FecHorSol) ORDER BY HOUR(FecHorSol)","",true).then((dataRes)=>{
            let Res: any = dataRes;
            //console.log(Res);
            this.DatGraHorD = Res.data;

            this.servicios.AccSobBDAA("SELECT","Si","HOUR(FecHorSol),COUNT(*),Estatus","","","viajes A","WHERE Estatus IN (|Cancelado Cliente|) AND FecHorSol BETWEEN |"+this.FecDes+"| AND (|"+this.FecHas+"| ¬ INTERVAL 1 DAY)","GROUP BY HOUR(FecHorSol) ORDER BY HOUR(FecHorSol)","",true).then((dataRes)=>{
              let Res: any = dataRes;
              //console.log(Res);
              this.DatGraHorE = Res.data;

              var Lab = this.lineChartLabels;
              var Dat = this.lineChartData;

              for (var i=0; i<this.DatGraHor.length; i++){
                for (var i2=0; i2<Lab.length; i2++){
                  if(Lab[i2] == this.DatGraHor[i][0]){
                    Dat[0].data[i2] = parseFloat(this.DatGraHor[i][1]);
                    this.lineChartData[0].data[i2] = parseFloat(this.DatGraHor[i][1]);
                  }
                }
              }

              for (var i=0; i<this.DatGraHorB.length; i++){
                for (var i2=0; i2<Lab.length; i2++){
                  if(Lab[i2] == this.DatGraHorB[i][0]){
                    Dat[1].data[i2] = parseFloat(this.DatGraHorB[i][1]);
                    this.lineChartData[1].data[i2] = parseFloat(this.DatGraHorB[i][1]);
                  }
                }
              }

              for (var i=0; i<this.DatGraHorC.length; i++){
                for (var i2=0; i2<Lab.length; i2++){
                  if(Lab[i2] == this.DatGraHorC[i][0]){
                    Dat[2].data[i2] = parseFloat(this.DatGraHorC[i][1]);
                    this.lineChartData[2].data[i2] = parseFloat(this.DatGraHorC[i][1]);
                  }
                }
              }

              for (var i=0; i<this.DatGraHorD.length; i++){
                for (var i2=0; i2<Lab.length; i2++){
                  if(Lab[i2] == this.DatGraHorD[i][0]){
                    Dat[3].data[i2] = parseFloat(this.DatGraHorD[i][1]);
                    this.lineChartData[3].data[i2] = parseFloat(this.DatGraHorD[i][1]);
                  }
                }
              }

              for (var i=0; i<this.DatGraHorE.length; i++){
                for (var i2=0; i2<Lab.length; i2++){
                  if(Lab[i2] == this.DatGraHorE[i][0]){
                    Dat[4].data[i2] = parseFloat(this.DatGraHorE[i][1]);
                    this.lineChartData[4].data[i2] = parseFloat(this.DatGraHorE[i][1]);
                  }
                }
              }

              if(this.Gra3 !== undefined){
                this.Gra3.ngOnDestroy();
                this.Gra3.chart = this.Gra3.getChartBuilder(this.Gra3.ctx);
              }

            }).catch((err)=>{console.log(err)});
          }).catch((err)=>{console.log(err)});
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }

  CarLisExp(){}
}